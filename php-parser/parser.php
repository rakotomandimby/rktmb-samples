<?php

require ('simple_html_dom.php');

$ch = curl_init("http://www.rktmb.org/");
$fp = fopen("rktmb.org.txt", "w");

curl_setopt($ch, CURLOPT_FILE, $fp);
curl_setopt($ch, CURLOPT_HEADER, 0);

curl_exec($ch);
curl_close($ch);

fclose($fp);

// Create DOM from URL or file

$html = file_get_html('rktmb.org.txt');

// Find all images
foreach($html->find('img') as $element) echo $element->src . '<br>';

// Find all links
foreach($html->find('a') as $element) echo $element->href . '<br>'; 

?>