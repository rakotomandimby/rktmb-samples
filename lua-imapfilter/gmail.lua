dofile"secret.lua"

--gmail=IMAP{
--server='imap.googlemail.com',
--username='rakotomandimby@gmail.com',
--password='lol',
--ssl='ssl3',
--}
--scp lua-imapfilter/gmail.lua partage:/home/mihamina/
--/usr/bin/imapfilter -v -c /home/mihamina/gmail.lua

-- gmail.INBOX:check_status()
-- ideoneov.INBOX:check_status()
-- gandi.INBOX:check_status()
-- ymail.INBOX:check_status()


yahoo.INBOX:check_status()

-- flinguer le system antispam de Yahoo:
-- bouger tout SPAM detecté dans INBOX
yahoo['Bulk Mail']:check_status()
yahoo_marked_spam=yahoo['Bulk Mail']:select_all()
yahoo_marked_spam:move_messages(yahoo.INBOX)

yahoo.INBOX:check_status()
netapsys.INBOX:check_status()


-- bouger les mail @ideoneov vers @netapsys
-- ideoneov.INBOX:move_messages(netapsys['INBOX'],ideoneov.INBOX:select_all())
-- bouger les mails GMail vers Gandi
-- gmail.INBOX:move_messages(gandi['INBOX'],gmail.INBOX:select_all())
-- gandi.INBOX:check_status()




function y_to(a)
   yahoo.INBOX:check_status()
   return yahoo.INBOX:contain_to(a)
end 

function y_cc(a)
   yahoo.INBOX:check_status()
   return yahoo.INBOX:contain_cc(a)
end 

function y_to_or_cc(a)
   yahoo.INBOX:check_status()
   return y_to(a)+y_cc(a)
end

function y_from(a)
   yahoo.INBOX:check_status()
   return yahoo.INBOX:contain_from(a)
end 

function y_field(y_header_name,y_header_value)
   yahoo.INBOX:check_status()
   return yahoo.INBOX:contain_field(y_header_name,y_header_value)
end      

function y_subject(a)
   yahoo.INBOX:check_status()
   return yahoo.INBOX:contain_subject(a)
end


function y_move(y_dest, y_cond)
   yahoo.INBOX:move_messages(y_dest,y_cond)
end

-- netapsys.INBOX:move_messages(netapsys['Servers/Bavard'],netapsys.INBOX:contain_to('www-data@ideotechnologies.com')*netapsys.INBOX:contain_subject('Undelivered Mail Returned to Sender'))
-- netapsys.INBOX:move_messages(netapsys['Servers/Bavard'],netapsys.INBOX:contain_from('munin@')*netapsys.INBOX:contain_subject('Danger php5'))
-- netapsys.INBOX:move_messages(netapsys['Servers/Bavard'],netapsys.INBOX:contain_from('root@gw')*netapsys.INBOX:contain_subject('/usr/bin/munin-cron'))
-- netapsys.INBOX:move_messages(netapsys['Servers'],netapsys.INBOX:contain_subject('lb02.cdvn')+netapsys.INBOX:contain_subject('lb01.cdvn')+netapsys.INBOX:contain_from('Script-bck')+netapsys.INBOX:contain_from('no-reply.privateCloud@ovh.net')+netapsys.INBOX:contain_to('uptimerobot@netapsys.fr')+netapsys.INBOX:contain_from('root@')+netapsys.INBOX:contain_from('mihamina@gateway')+netapsys.INBOX:contain_from('mihamina@gw')+netapsys.INBOX:contain_from('mihamina@partage')+netapsys.INBOX:contain_from('munin@')+netapsys.INBOX:contain_from('cfengine@')+netapsys.INBOX:contain_from('nagios@')+netapsys.INBOX:contain_from('dev53@')+netapsys.INBOX:contain_from('orange.mg.netapsys.fr')+netapsys.INBOX:contain_from('alert@rblmon.com')+netapsys.INBOX:contain_from('dev52@'))
-- netapsys.INBOX:move_messages(netapsys['Servers/IC'],netapsys.INBOX:contain_subject('Jenkins build is still unstable'))
-- 
-- netapsys.INBOX:move_messages(netapsys['Interne/Wiki/Partage'],(netapsys.INBOX:contain_from('noreply@ideotechnologies.com')*(netapsys.INBOX:contain_body('has just shared')+netapsys.INBOX:contain_body('vient de partager'))))
-- netapsys.INBOX:move_messages(netapsys['Interne/Wiki/Comment'],(netapsys.INBOX:contain_from('noreply@ideotechnologies.com')*netapsys.INBOX:contain_body('comment added')))
-- netapsys.INBOX:move_messages(netapsys['Interne/Wiki/Update'],netapsys.INBOX:contain_from('noreply@ideotechnologies.com'))
-- netapsys.INBOX:move_messages(netapsys['Interne/Wiki/Inside'],netapsys.INBOX:contain_from('news-admin-inside@netapsys.fr'))




y_move(yahoo['RKTMB/OVH'],y_field('List-Post','ml.ovh.net')+y_to('ml.ovh.net'))

y_move(yahoo['RKTMB/Codeurs'],y_from('codeur.com')+y_from('freelancer.com'))
y_move(yahoo['RKTMB/RHIEN'],y_to('riha@cliprezo.net')+y_field('List-Id','rhien.rhien.org'))
y_move(yahoo['RKTMB/Social/Zorpia'],y_from('zorpia.com'))
y_move(yahoo['RKTMB/Social/Trombi'],y_from('trombi.com'))
y_move(yahoo['RKTMB/Social/Viadeo'],y_from('viadeo.com'))
y_move(yahoo['RKTMB/Social/LinkedIn'],y_from('linkedin.com'))
y_move(yahoo['RKTMB/Social/FaceBook'],y_from('facebookmail.com')+y_from('facebook.com'))
y_move(yahoo['RKTMB/Social/GooglePlus'],y_from('plus.google.com'))
y_move(yahoo['RKTMB/News/Computer'],(y_from('contact@linuxfoundation.org')+y_from('newsletter@materiel.net')+y_from('info@fnac.com')+y_from('zdnet.online.com')+y_from('zdnet.fr')+y_from('techrepublic')+y_from('rueducommerce')+y_from('ldlc.')))
y_move(yahoo['RKTMB/Renater'],y_field('List-Id','renater.fr'))
y_move(yahoo['RKTMB/News/Libre'],y_field('List-Id','libreplanet.org'))
y_move(yahoo['RKTMB/Afnog'],y_field('List-Id','afnog.afnog.org'))

y_move(yahoo['Distribution/Debian'],(y_to_or_cc('lists.debian.org')+y_from('bugs.debian.org')+y_to_or_cc('bugs.debian.org')))
y_move(yahoo['Distribution/Arch'],y_to_or_cc('archlinux.org'))
y_move(yahoo['Distribution/Gentoo'],y_to('gentoo.org'))
y_move(yahoo['Distribution/Fedora'],(y_to_or_cc('fedoraproject.org')+y_to_or_cc('packagers.lists.repoforge.org')+y_to_or_cc('tools.lists.repoforge.org')+y_to_or_cc('users.lists.repoforge.org')+y_to_or_cc('ius-community.lists.launchpad.net')+y_to_or_cc('atrpms.net')+y_to_or_cc('epel-devel-list.redhat.com')))
y_move(yahoo['Distribution/Centos'],(y_to('centos.org')+y_cc('centos.org')+y_to('elrepo.org')+y_to('rhelv6-list.redhat.com')+y_to('jpackage-discuss.zarb.org')))
y_move(yahoo['Distribution/Ubuntu'],y_to('lists.ubuntu.com'))
y_move(yahoo['Distribution/FreeBSD'],y_to('.freebsd.org')+y_to('lists.freenas.org')+y_cc('lists.freenas.org'))
y_move(yahoo['Distribution/SciLinux'],(y_to('scientific-linux-users*fnal.gov')+y_cc('scientific-linux-users*fnal.gov')))
y_move(yahoo['Distribution/OpenIndiana'],y_to('openindiana-discuss.openindiana.org'))
y_move(yahoo['Distribution/PcBSD'],y_to('.pcbsd.org'))
y_move(yahoo['Distribution/Mandriva'],(y_to('om-cooker-openmandriva.org')+y_to('mandrivalinux.org')+y_to('cooker.mandrivalinux.org'))) 

y_move(yahoo['Launchpad/Midori'],y_field('X-Launchpad-Bug','product=midori'))

y_move(yahoo['Security'],(y_to('securityfocus.com')+y_cc('securityfocus.com')+y_to('securityfocus.com')+y_to('full-disclosure.lists.grok.org.uk')+y_to('funsec.linuxbox.org')+y_to('seclists.org')))
y_move(yahoo['Patterns'],y_to('patterns-discussion.cs.uiuc.edu'))
y_move(yahoo['Scrum'],(y_to('scrumdevelopment.yahoogroups.com')+y_field('List-ID','scrumalliance.googlegroups.com')+y_to('agileprojectmanagement.yahoogroups.com')))
y_move(yahoo['VoIP'],y_to_or_cc('voip@ckts.info'))

y_move(yahoo['Spam'],y_subject('GoBuzz.Me'))


y_move(yahoo['Softwares/Prestashop'],y_subject('[Prestashop]'))
y_move(yahoo['Softwares/Kamailo'],(y_to_or_cc('lists.sip-router.org')))
y_move(yahoo['Softwares/SquirrelSQL'],(y_to('squirrel-sql-users@lists.sourceforge.net')+y_cc('squirrel-sql-users@lists.sourceforge.net')))
y_move(yahoo['Softwares/PGSQL'],(y_to_or_cc('postgresql.org')))
y_move(yahoo['Softwares/SystemD'],(y_to('systemd-devel@lists.freedesktop.org')+y_cc('systemd-devel@lists.freedesktop.org')))
y_move(yahoo['Softwares/Maven'],(y_to('users@maven.apache.org')+y_cc('users@maven.apache.org')))
y_move(yahoo['Softwares/Sonar'],(y_to('sonar.codehaus.org')+y_cc('sonar.codehaus.org')))
y_move(yahoo['Softwares/Perl'],(y_to('beginners@perl.org')+y_cc('beginners@perl.org')))
y_move(yahoo['Softwares/Jenkins'],(y_to('jenkinsci-users@googlegroups.com')+y_cc('jenkinsci-users@googlegroups.com')+y_to('jenkinsci-dev@googlegroups.com')+y_cc('jenkinsci-dev@googlegroups.com')))
y_move(yahoo['Softwares/CfEngine'],(y_field('List-ID','help-cfengine.googlegroups.com')+y_to('help-cfengine@googlegroups.com')+y_cc('help-cfengine@googlegroups.com')+y_to('core@noreply.github.com')))
y_move(yahoo['Softwares/Varnish'],y_to('varnish'))
y_move(yahoo['Softwares/Samba'],y_to_or_cc('lists.samba.org')+y_to_or_cc('samba-technical@samba.org'))
y_move(yahoo['Softwares/VMware'],y_from('communities-emailer@vmware.com'))
y_move(yahoo['Softwares/JSTools'],y_field('List-ID','js-tools.googlegroups.com'))
y_move(yahoo['Softwares/Cornelios'],y_field('List-ID','cornelios.googlegroups.com'))
y_move(yahoo['Softwares/Solr'],y_to('solr-user.lucene.apache.org'))
y_move(yahoo['Softwares/Wine'],y_to('wine-users.winehq.org'))
y_move(yahoo['Softwares/OpenVPN'],y_to('openvpn-users.lists.sourceforge.net'))
y_move(yahoo['Softwares/Tomcat'],y_to('users.tomcat.apache.org'))
y_move(yahoo['Softwares/Pumgrana'],y_field('List-ID','pumgrana.googlegroups.com'))
y_move(yahoo['Softwares/Phantomjs'],(y_to('phantomjs@googlegroups.com')+y_cc('phantomjs@googlegroups.com')))
y_move(yahoo['Softwares/Zend'],(y_to('lists.zend.com')+y_cc('lists.zend.com')+y_field('Mailing-List','lists.zend.com')))
y_move(yahoo['Softwares/Icinga'],y_to('icinga-'))
y_move(yahoo['Softwares/Capistrano'],y_field('List-ID','capistrano.googlegroups.com'))
y_move(yahoo['Softwares/Apache'],y_to('httpd.apache.org'))
y_move(yahoo['Softwares/OVirt'],y_to('users.ovirt.org'))
y_move(yahoo['Softwares/GLPI'],(y_to('glpi-user.gna.org')+y_to('glpi-dev.gna.org')))
y_move(yahoo['Softwares/OpenMeetings'],(y_to('openmeetings-dev.incubator.apache.org')+y_to('openmeetings-user.incubator.apache.org')+y_to('user.openmeetings.apache.org')+y_to('dev.openmeetings.apache.org')))
y_move(yahoo['Softwares/SVN'],y_to('users.subversion.apache.org'))
y_move(yahoo['Softwares/Pidgin'],y_to('pidgin.im'))
y_move(yahoo['Softwares/JMeter'],y_to('user.jmeter.apache.org'))
y_move(yahoo['Softwares/Pencil'],y_to('pencil-user.googlegroups.com'))
y_move(yahoo['Softwares/PhoneGap'],(y_to('callback-dev.incubator.apache.org')+y_to('phonegap.googlegroups.com')))
y_move(yahoo['Softwares/BackupPC'],y_to('backuppc-users.lists.sourceforge.net'))
y_move(yahoo['Softwares/Elastix'],y_to('elastix.org'))
y_move(yahoo['Softwares/FFMpeg'],(y_to('ffmpeg.org')+y_to('libav.org')))
y_move(yahoo['Softwares/MySQL'],(y_to('mysql.com')+y_to('maria-discuss.lists.launchpad.net')))
y_move(yahoo['Softwares/LXDE'],(y_to('openbox.icculus.org')+y_subject('[Lxde-list]')))
y_move(yahoo['Softwares/Bash'],y_to('help-bash.gnu.org'))
y_move(yahoo['Softwares/Xdebug'],y_field('X-list','xdebug-general'))
y_move(yahoo['Softwares/FreeSwitch'],y_to('freeswitch.org'))
y_move(yahoo['Softwares/Jelix'],y_to('jelix-fr.googlegroups.com'))
y_move(yahoo['Softwares/Kde'],(y_to('kde.org')+y_to('qt.nokia.com')+y_to('kdevelop.barney.cs.uni-potsdam.de')))
y_move(yahoo['Softwares/LightDM'],y_to('lightdm.lists.freedesktop.org'))
y_move(yahoo['Softwares/Planner'],y_to('planner-dev-list.gnome.org'))
y_move(yahoo['Softwares/StatusNet'],y_from('@status.net'))
y_move(yahoo['Softwares/Mercurial'],(y_to('mercurial.selenic.com')+y_to('mercurial-devel.selenic.com')+y_to('thg-dev.googlegroups.com')))
y_move(yahoo['Softwares/Libreoffice'],y_to('users.global.libreoffice.org'))
y_move(yahoo['Softwares/Symfony'],(y_field('List-ID','symfony1.googlegroups.com')+y_field('List-ID','symfony2.googlegroups.com')+y_field('List-ID','doctrine-user.googlegroups.com')))
y_move(yahoo['Softwares/NodeJS'],y_field('List-ID','nodejs.googlegroups.com'))
y_move(yahoo['Softwares/Squid'],y_to('squid-cache.org'))
y_move(yahoo['Softwares/Drupal'],y_to('drupal.org'))
y_move(yahoo['Softwares/Evince'],y_to('evince-list.gnome.org'))
y_move(yahoo['Softwares/Poppler'],y_to('poppler.lists.freedesktop.org'))
y_move(yahoo['Softwares/Mozilla'],y_to('lists.mozilla.org'))
y_move(yahoo['Softwares/Monit'],y_to('monit-general.nongnu.org'))
y_move(yahoo['Softwares/Bind'],(y_to('bind-users.lists.isc.org')+y_to('bind10-users.lists.isc.org')+y_to('bind-announce.lists.isc.org')))
y_move(yahoo['Softwares/Emacs'],(y_to('php-mode@noreply.github.com')+y_to('help-gnu-emacs.gnu.org')+y_to('emacs-nxml-mode')+y_to('ecb-list')))
y_move(yahoo['Softwares/Spamassassin'],y_to('users.spamassassin.apache.org'))
y_move(yahoo['Softwares/LXC'],y_to('lxc-users.lists.sourceforge.net')+y_to('linuxcontainers.org'))
y_move(yahoo['Softwares/Android'],y_to('android-discuss.googlegroups.com'))
y_move(yahoo['Softwares/Gnumeric'],y_to('gnumeric-list.gnome.org'))
y_move(yahoo['Softwares/Netflow'],y_to('netflow-tools.lists.sourceforge.net'))
y_move(yahoo['Softwares/Clamav'],y_to('clamav-users.lists.clamav.net'))
y_move(yahoo['Softwares/Exim'],y_to('exim.org'))
y_move(yahoo['Softwares/Python'],y_to('python-list@python.org')+y_cc('python-list@python.org'))
y_move(yahoo['Softwares/RSyslog'],y_to('rsyslog.lists.adiscon.com'))
y_move(yahoo['Softwares/OpenWRT'],y_to('openwrt-users.lists.openwrt.org'))
y_move(yahoo['Softwares/NetSNMP'],y_to('net-snmp-users.lists.sourceforge.net'))
y_move(yahoo['Softwares/DansGuardian'],y_to('dansguardian.yahoogroups.com'))
y_move(yahoo['Softwares/Quagga'],y_to('quagga-users.lists.quagga.net'))
y_move(yahoo['Softwares/Netdev'],y_field('List-ID','netdev.vger.kernel.org'))
y_move(yahoo['Softwares/Kvm'],(y_field('List-ID','kvm.vger.kernel.org')+y_to('libvirt-users.redhat.com')+y_to('virt-tools-list.redhat.com')+y_to('libvir-list.redhat.com')))
y_move(yahoo['Softwares/Yum'],y_to('yum.lists.baseurl.org'))
y_move(yahoo['Softwares/Netfilter'],y_field('List-ID','netfilter.vger.kernel.org'))
y_move(yahoo['Softwares/Git'], (y_field('List-ID','gitolite.googlegroups.com')+y_field('List-ID','git.vger.kernel.org')+y_to('gitlabhq.googlegroups.com')+y_to('gitorious.googlegroups.com')+y_to('bitbucket-users.googlegroups.com')+y_field('List-ID','msysgit')))
y_move(yahoo['Softwares/NetBeans'],y_to('netbeans.org'))
y_move(yahoo['Softwares/Haskell'],y_to('haskell.org'))
y_move(yahoo['Softwares/Ocaml'],y_to('caml-list.inria.fr')+y_to('lablgtk-list.lists.forge.ocamlcore.org')+y_to('ocaml_beginners.yahoogroups.com')+y_to('ocamlnet-devel.lists.sourceforge.net'))
y_move(yahoo['Softwares/XFCE'],y_to('xfce.xfce.org')+y_cc('xfce.xfce.org'))
y_move(yahoo['Softwares/Ocsigen'],y_to('ocsigen.sympa.mancoosi.univ-paris-diderot.fr')+y_to('ocsigen.inria.fr'))
y_move(yahoo['Softwares/Munin'],y_to('munin-users.lists.sourceforge.net'))
y_move(yahoo['Softwares/GRUB'],y_to('help-grub.gnu.org'))
y_move(yahoo['Softwares/Xen'],y_to('xen.org'))
y_move(yahoo['Softwares/OpenNebula'],y_to('users-opennebula.org'))
y_move(yahoo['Softwares/RpPPPOE'],y_to('rp-pppoe'))
y_move(yahoo['Softwares/CoreUtils'],y_to('coreutils.gnu.org'))
y_move(yahoo['Softwares/AutoMake'],y_to('automake.gnu.org'))
y_move(yahoo['Softwares/Lua'],y_to('lua-l-lists.lua.org'))
y_move(yahoo['Softwares/Sogo'],(y_to('users.sogo.nu')+y_to('sogo.nu')+y_to('sogo@noreply.github.com'))+(y_to('lists.openchange.org')+y_cc('lists.openchange.org')))
y_move(yahoo['Softwares/Cordova'],(y_to('cordova')+y_to('phonegap')))
y_move(yahoo['Softwares/Lftp'],(y_to('lftp.uniyar.ac.ru')+y_to('lftp-devel.uniyar.ac.ru')))
y_move(yahoo['Softwares/RedNoteBook'],y_to('rednotebook.lists.launchpad.net'))
y_move(yahoo['Softwares/PHP'],(y_to('.lists.php.net')+y_to('phpmyadmin')+y_field('List-ID','google-api-php-client.googlegroups.com')))
y_move(yahoo['Softwares/Meld'],y_to('meld-list.gnome.org'))
y_move(yahoo['Softwares/Linux'],(y_field('List-ID','linux.vger.kernel.org')+y_field('List-ID','linux-ext4.vger.kernel.org')+y_to('vger.kernel.org')))

