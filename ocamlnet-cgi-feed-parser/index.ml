open Netcgi

module T = Netcgi_modtpl;;

let template = T.template "template.html";;

(* Build page to "output" buffer*)
let display_form (cgi:cgi) =
  template#set "message" "";
  template#set "self" (cgi#url());
  template#output cgi;;

(* flush output to web client*)
let () =
  let buffered _ ch = new Netchannels.buffered_trans_channel ch in
    Netcgi_cgi.run ~output_type:(`Transactional buffered) display_form
      
