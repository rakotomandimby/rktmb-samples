<html>
<head></head>
<body>
<?
include("Product.class.php");

function query_database($query)                      //       
{                                                    //  All this is
  $dbh = new PDO ('sqlite:/var/www/database.db');    //  duplicate code 
  $result = $dbh->query($query);                     //  in Users.class.php
  $rows= $result->fetchAll();                        //
  return $rows;                                      //
}                                                    //  Dont do this in real life!
                                                     // 
function exec_database($to_exec)                     // 
{                                                    // 
  $dbh = new PDO ('sqlite:/var/www/database.db');    // 
  $result = $dbh->exec($to_exec);                    //
  return $result;                                    // 
}                                                    //      

$query_all_products="SELECT id FROM products";
$products_id_row=query_database($query_all_products);


foreach($products_id_row as $database_product)
  {
    $product = new Product($database_product['id']);
    $product->set_from_database();
    print "<p>";
    $product->to_html();
    print "</p>";

  }
?>

</body>
</html>