<html>
<head></head>
<body>
<?
include("Product.class.php");

function query_database($query)                      //       
{                                                    //  All this is
  $dbh = new PDO ('sqlite:/var/www/database.db');    //  duplicate code 
  // print $query ."<br />" ; 
  $result = $dbh->query($query);                     //  in Users.class.php
  $rows= $result->fetchAll();                        //
  return $rows;                                      //
}                                                    //  Dont do this in real life!
                                                     // 
function exec_database($to_exec)                     // 
{                                                    // 
  $dbh = new PDO ('sqlite:/var/www/database.db');    // 
  $result = $dbh->exec($to_exec);                    //
  return $result;                                    // 
}                                                    //      

$total_number_of_products = count(query_database("SELECT id FROM products"));
$products_per_page=5;

if(!isset($_REQUEST['page']))
  { $_REQUEST['page']=0; }

$max_element = $total_number_of_products - ($_REQUEST['page'] * $products_per_page);
$min_element = $total_number_of_products - (($_REQUEST['page'] + 1 ) * $products_per_page)  ;
print $total_number_of_products .": ". $min_element ."->". $max_element . "<br />";
$query_page_products="SELECT id FROM products WHERE id > ".$min_element." AND id < ".$max_element."";
$products_id_row=query_database($query_page_products);

foreach($products_id_row as $database_product)
  {
    $product = new Product($database_product['id']);
    $product->set_from_database();
    print "<p>";
    $product->to_html();
    print "</p>";     
  }

?>
<a href="?page=<? print $_REQUEST['page']+1; ?>"> aller a la page <? print $_REQUEST['page']+2; ?>, qui est la suivante.</a>
</body>
</html>