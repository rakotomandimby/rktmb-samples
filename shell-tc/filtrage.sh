#!/bin/bash


awk -F "," '{print $1,$2,$3,$4}' < /etc/sysconfig/users.txt > /tmp/users


${IPT} -t filter -F OUTPUT
${IPT} -t filter -F INPUT
${IPT} -t filter -F FORWARD 
${IPT} -t filter -F BADDNS ||  ${IPT} -t filter -N BADDNS 
# ${IPT} -t filter -F SMTP  ||  ${IPT} -t filter -N SMTP 


. /etc/sysconfig/conf-bandwidth.sh # charger la configuration du bandwidth manager


${IPT}  -t filter -A FORWARD -p tcp --dport 53 -s 192.168.0.0/24 -j BADDNS
${IPT}  -t filter -A FORWARD -p udp --dport 53 -s 192.168.0.0/24 -j BADDNS

for E_DNS in ${OK_EXT_DNS} 
do
    ${IPT} -t filter -A BADDNS -s ${E_DNS} -j RETURN
done


${IPT} -t filter -A BADDNS -j LOG --log-prefix "baddns: "
${IPT} -t filter -A BADDNS -j DROP



# ${IPT} -t filter -A FORWARD -p tcp --dport 25 -s ${LAN_RANGE} -j SMTP
# ${IPT} -t filter -A SMTP -d 80.247.227.141 -j RETURN
# ${IPT} -t filter -A SMTP -j LOG --log-prefix "badsmtp: "
# ${IPT} -t filter -A SMTP -j DROP

# Mamisoa VM
${IPT} -t filter -A FORWARD -s 192.168.0.199/32  -i ${IFLAN} -j ACCEPT 

while read ONE_PRIO ONE_NOM ONE_MAC ONE_IP
do
    ${IPT} -t filter -A FORWARD -s ${ONE_IP}/32 -m mac --mac-source ${ONE_MAC} -i ${IFLAN} -j ACCEPT # prio ${ONE_PRIO} - ${ONE_NOM}
done < /tmp/users


# Forward du Nat pour la VM Java Tomcat port 8080
${IPT} -t filter -A FORWARD  -p tcp -i ${IFWAN} -d 41.190.237.66 --dport 8080   -j LOG  --log-prefix "javanat: "
${IPT} -t filter -A FORWARD  -p tcp -i ${IFWAN} -s 82.124.29.246  -d 41.190.237.66 --dport 8080   -j ACCEPT # puteaux
# echo ${IPT} -t filter -A FORWARD  -p tcp -i ${IFWAN} -s 80.247.227.141 -d 41.190.237.66 --dport 8080   -j ACCEPT # recette

${IPT} -t filter -A INPUT -p udp -i ${IFWAN} -d 41.190.237.66 --dport 53 -j ACCEPT
${IPT} -t filter -A INPUT -p tcp -i ${IFWAN} -d 41.190.237.66 --dport 80 -j ACCEPT
${IPT} -t filter -A INPUT -p tcp -i ${IFWAN} -d 41.190.237.66 --dport 22 -j ACCEPT
${IPT} -t filter -A INPUT -p tcp -i ${IFWAN} -d 41.190.237.66 --dport 2222 -j ACCEPT

${IPT} -t filter -A FORWARD  -i ${IFLAN} -j LOG  --log-prefix "franck: "
${IPT} -t filter -A FORWARD  -i ${IFLAN} -j DROP

${IPT} -t filter -A INPUT -i ${IFLAN}  -j ACCEPT

${IPT} -t filter -A INPUT -i ${IFWAN} -m state --state ESTABLISHED,RELATED -j ACCEPT

${IPT} -t filter -A INPUT  -i  ${IFLAN}  -j REJECT
${IPT} -t filter -A INPUT  -i  ${IFWAN}  -j REJECT


