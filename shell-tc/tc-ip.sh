for IP_LAN in $(awk '{print $4}' < /tmp/users)
do
    LAST=$(echo  ${IP_LAN} | awk -F '.' '{print $4}')
    CLASS_ID=$(printf "a%.3X" ${LAST} | tr [A-Z] [a-z])
    ${TC} class add dev ${IFLAN} parent 1:1 classid 1:${CLASS_ID} htb  rate $(( ${DOWN_PAR_IP} / 100 ))kbit ceil ${DOWN_PAR_IP}kbit prio 7 quantum 1500
    ${TC} class add dev ${IFWAN} parent 1:1 classid 1:${CLASS_ID} htb  rate $(( ${UP_PAR_IP} / 100 ))kbit   ceil ${UP_PAR_IP}kbit   prio 7 quantum 1500    
done
