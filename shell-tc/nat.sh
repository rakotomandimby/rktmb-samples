#!/bin/bash

${IPT} -t nat -F POSTROUTING
${IPT} -t nat -F PREROUTING

# Nat IP publique
${IPT} -t nat -A POSTROUTING -s ${LAN_RANGE} ! -d ${LAN_RANGE} -j SNAT --to-source 41.190.237.66

# NAT entrant Java
${IPT} -t nat -A PREROUTING -p tcp -i eth1 -d 41.190.237.66 --dport 8080 -j DNAT --to 192.168.0.10:8080 # Java server

# NAT entrant Java
${IPT} -t nat -A PREROUTING -p tcp -i eth1 -d 41.190.237.66 --dport 2222 -j DNAT --to 192.168.0.3:22 # IPBX server


