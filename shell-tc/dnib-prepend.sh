#!/bin/bash

echo '$TTL    60                                                                             ' 
echo '0.168.192.in-addr.arpa.  IN      SOA     mg.ideoneov.com.     root.mg.ideoneov.com.  ( '
echo '                                                  '$(date +%Y%m%d%H)'                  '
echo '                                                  604800                               '
echo '                                                  86400                                '
echo '                                                  2419200                              '
echo '                                                  604800                               '
echo ')                                                                                      '    
echo '        IN       NS     mg.ideoneov.com.                                               '
echo ''


