#!/bin/bash

cd /etc/munin/plugins
rm -f in_*
awk -F ',' '{print "ln -s /usr/local/bin/in_ in_"$4}' /etc/sysconfig/users.txt | bash
service munin-node restart
