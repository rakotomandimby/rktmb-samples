#!/bin/bash

/usr/bin/rsync 192.168.0.9::bin/bind-prepend.sh /usr/local/bin/
/usr/bin/rsync 192.168.0.9::bin/bind-body.sh /usr/local/bin/

/usr/bin/rsync 192.168.0.9::bin/dhcpd-prepend.sh /usr/local/bin/
/usr/bin/rsync 192.168.0.9::bin/dhcpd-body.sh /usr/local/bin/
/usr/bin/rsync 192.168.0.9::bin/dhcpd-apend.sh /usr/local/bin/

/usr/bin/rsync 192.168.0.9::bin/dnib-prepend.sh /usr/local/bin/
/usr/bin/rsync 192.168.0.9::bin/dnib-body.sh /usr/local/bin/

/usr/bin/rsync 192.168.0.9::bin/tc-show.sh /usr/local/bin/

/usr/bin/rsync 192.168.0.9::bin/tc-rules.sh /usr/local/bin/
/usr/bin/rsync 192.168.0.9::bin/tc-ip.sh /usr/local/bin/
/usr/bin/rsync 192.168.0.9::bin/ipt-ip.sh /usr/local/bin/
/usr/bin/rsync 192.168.0.9::bin/google.txt /usr/local/bin/

/usr/bin/rsync 192.168.0.9::bin/filtrage.sh /usr/local/bin/
/usr/bin/rsync 192.168.0.9::bin/debit.sh /usr/local/bin/

/usr/bin/rsync 192.168.0.9::etc/conf-bandwidth.sh /etc/network/
/usr/bin/rsync 192.168.0.9::etc/users.txt /etc/network/

/usr/local/bin/generate-configuration.sh > /dev/null

