


${TC} class add dev ${IFLAN} parent 1:1 classid 1:80 htb rate $(( ${DL_RATE} *  2 / 100 ))kbit ceil $(( ${DL_RATE} *  90 / 100 ))kbit prio 8 quantum 1500
${TC} class add dev ${IFLAN} parent 1:1 classid 1:60 htb rate $(( ${DL_RATE} *  5 / 100 ))kbit ceil $(( ${DL_RATE} *  30 / 100 ))kbit prio 6 quantum 1500 # Mail et Google
${TC} class add dev ${IFLAN} parent 1:1 classid 1:50 htb rate $(( ${DL_RATE} *  5 / 100 ))kbit ceil $(( ${DL_RATE} *  70 / 100 ))kbit prio 5 quantum 1500 # SSH
${TC} class add dev ${IFLAN} parent 1:2 classid 1:40 htb rate              ${TOIP_DL_RATE}kbit ceil               ${TOIP_DL_RATE}kbit prio 1 quantum 1500 # TOIP
${TC} class add dev ${IFLAN} parent 1:1 classid 1:30 htb rate $(( ${DL_RATE} *  5 / 100 ))kbit ceil $(( ${DL_RATE} *  50 / 100 ))kbit prio 3 quantum 1500 # Wiki Srv VPN
${TC} class add dev ${IFLAN} parent 1:1 classid 1:20 htb rate $(( ${DL_RATE} *  5 / 100 ))kbit ceil $(( ${DL_RATE} *  60 / 100 ))kbit prio 2 quantum 1500 # VIP
${TC} class add dev ${IFLAN} parent 1:1 classid 1:10 htb rate $(( ${DL_RATE} *  5 / 100 ))kbit ceil $(( ${DL_RATE} *  50 / 100 ))kbit prio 1 quantum 1500 # Skype


${TC} class add dev ${IFWAN} parent 1:1 classid 1:80 htb rate $(( ${UL_RATE} *  2 / 100 ))kbit ceil $(( ${UL_RATE} *  90 / 100 ))kbit prio 8 quantum 1500
${TC} class add dev ${IFWAN} parent 1:1 classid 1:60 htb rate $(( ${UL_RATE} *  5 / 100 ))kbit ceil $(( ${UL_RATE} *  60 / 100 ))kbit prio 6 quantum 1500 # Mail et Google
${TC} class add dev ${IFWAN} parent 1:1 classid 1:50 htb rate $(( ${UL_RATE} *  5 / 100 ))kbit ceil $(( ${UL_RATE} *  70 / 100 ))kbit prio 5 quantum 1500 # SSH
${TC} class add dev ${IFWAN} parent 1:2 classid 1:40 htb rate              ${TOIP_UL_RATE}kbit ceil               ${TOIP_UL_RATE}kbit prio 1 quantum 1500 # TOIP
${TC} class add dev ${IFWAN} parent 1:1 classid 1:30 htb rate $(( ${UL_RATE} *  5 / 100 ))kbit ceil $(( ${UL_RATE} *  50 / 100 ))kbit prio 3 quantum 1500 # Wiki Srv VPN
${TC} class add dev ${IFWAN} parent 1:1 classid 1:20 htb rate $(( ${UL_RATE} *  5 / 100 ))kbit ceil $(( ${UL_RATE} *  60 / 100 ))kbit prio 2 quantum 1500 # VIP
${TC} class add dev ${IFWAN} parent 1:1 classid 1:10 htb rate $(( ${UL_RATE} *  5 / 100 ))kbit ceil $(( ${UL_RATE} *  50 / 100 ))kbit prio 1 quantum 1500 # Skype

