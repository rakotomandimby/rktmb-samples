#!/bin/bash 


export IFLAN="eth0"
export IFWAN="eth1"

export TC="/sbin/tc"
export IPT="/sbin/iptables"
export ETHTOOL="/usr/sbin/ethtool"
export PRIPS="/usr/bin/prips"

export TOIP_DL_RATE="300"
export TOIP_UL_RATE="300"

export DL_RATE="4000" 
export UL_RATE="4000" 

export DL_RATE=$(( ${DL_RATE} - ${TOIP_DL_RATE} ))
export UL_RATE=$(( ${UL_RATE} - ${TOIP_UL_RATE} )) 

export DOWN_PAR_IP="600"
export UP_PAR_IP="600"

export LAN_RANGE="192.168.0.0/24"

# supprimer les lignes vides inplace
sed -i '/^$/d' /etc/sysconfig/users.txt


# sudo iptables -t nat -A POSTROUTING -s 192.168.0.0/24 ! -d 192.168.0.0/24 -j SNAT --to-source 41.190.237.66
# sudo iptables -t filter -F INPUT
# sudo iptables -t filter -F FORWARD 
# sudo tc qdisc del dev eth1 root; sudo tc qdisc del dev eth0 root;

# configuration DHCP
/usr/local/bin/dhcpd-prepend.sh                 >  /etc/dhcp/dhcpd.conf
/usr/local/bin/dhcpd-body.sh                    >> /etc/dhcp/dhcpd.conf
/usr/local/bin/dhcpd-apend.sh                   >> /etc/dhcp/dhcpd.conf

service dhcpd restart

# Bind forward
/usr/local/bin/bind-prepend.sh "neov.mg"         >  /etc/named/neov.mg.dns
/usr/local/bin/bind-body.sh    "neov.mg"         >> /etc/named/neov.mg.dns
/usr/local/bin/bind-prepend.sh "mg.ideoneov.com" >  /etc/named/mg.ideoneov.com.dns
/usr/local/bin/bind-body.sh    "mg.ideoneov.com" >> /etc/named/mg.ideoneov.com.dns
# Bind reverse
/usr/local/bin/dnib-prepend.sh                   > /etc/named/mg.ideoneov.com.inv # /etc/named/neov.mg.inv
/usr/local/bin/dnib-body.sh                      >> /etc/named/mg.ideoneov.com.inv

service named restart

# Recconfigure monitoring
/usr/local/bin/monitoring.sh 
echo "OK: Reconfigure monitoring"

echo "Entering /usr/local/bin/nat.sh"
# Nat : SNAT et DNAT
/usr/local/bin/nat.sh     
echo "OK: SNAT et DNAT"

echo "Entering /usr/local/bin/debit.sh"
# Bandwidth Management
/usr/local/bin/debit.sh  
echo "OK: Bandwidth Management"

echo "Entering /usr/local/bin/filtrage.sh"
# MAC restrictions
/usr/local/bin/filtrage.sh 
echo "OK: MAC restrictions" 

echo "Entering /usr/local/bin/accounting.sh DOWN"
# regles d'accounting
/usr/local/bin/accounting.sh down
echo "OK: accounting.sh down " 

echo "Entering /usr/local/bin/accounting.sh UP"
/usr/local/bin/accounting.sh up
echo "OK: accounting.sh up " 



