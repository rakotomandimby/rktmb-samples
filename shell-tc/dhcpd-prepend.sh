#!/bin/bash

echo '###### ATTENTION FRANCK! FICHIER AUTOGENERE ! ############'
echo 'authoritative;                                            '
echo 'ddns-update-style interim;                                '
echo 'ignore client-updates;                                    '
echo 'update-static-leases on;                                  '
echo 'option routers 192.168.0.9;                               '
echo 'option domain-name "mg.ideoneov.com";                     '
echo 'option domain-name-servers 192.168.0.9;                   '
echo 'default-lease-time 86400;                                 '
echo 'interfaces="eth0";                                        '
echo 'deny unknown-clients;                                     '
echo '                                                          '
echo 'subnet 192.168.0.0 netmask 255.255.0.0 {                  '
echo '        range 192.168.0.11 192.168.0.254;                 '
echo '        option broadcast-address 192.168.0.255;           '

