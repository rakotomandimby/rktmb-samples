for IP_LAN in $(awk '{print $4}' < /tmp/users)
do
    LAST=$(echo  ${IP_LAN} | awk -F '.' '{print $4}')
    CLASS_ID=$(printf "a%.3X" ${LAST} | tr [A-Z] [a-z])
    ${IPT} -t mangle -A FORWARD -d ${IP_LAN}  -j CLASSIFY --set-class 1:${CLASS_ID}
    ${IPT} -t mangle -A FORWARD -s ${IP_LAN}  -j CLASSIFY --set-class 1:${CLASS_ID}
done
