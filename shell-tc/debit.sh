#!/bin/bash


awk -F "," '{print $1,$2,$3,$4}' < /etc/sysconfig/users.txt > /tmp/users


${ETHTOOL} -K ${IFLAN} gso off

# pre run loop

${IPT} -t mangle -F FORWARD 
${IPT} -t mangle -F POSTROUTING
${IPT} -t mangle -F PREROUTING
${IPT} -t mangle -F OUTPUT
${IPT} -t mangle -F INPUT


${TC} qdisc del dev ${IFLAN} root
${TC} qdisc del dev ${IFWAN} root

${TC} qdisc add dev ${IFLAN} root handle 1:0 htb default 80
${TC} class add dev ${IFLAN} parent 1:0 classid 1:1 htb rate ${DL_RATE}kbit      ceil ${DL_RATE}kbit
${TC} class add dev ${IFLAN} parent 1:0 classid 1:2 htb rate ${TOIP_DL_RATE}kbit ceil ${TOIP_DL_RATE}kbit

${TC} qdisc add dev ${IFWAN} root handle 1:0 htb default 80
${TC} class add dev ${IFWAN} parent 1:0 classid 1:1 htb rate ${UL_RATE}kbit      ceil ${UL_RATE}kbit
${TC} class add dev ${IFWAN} parent 1:0 classid 1:2 htb rate ${TOIP_UL_RATE}kbit ceil ${TOIP_UL_RATE}kbit


echo "  chargement des configurations"
. /etc/sysconfig/conf-bandwidth.sh # charger la configuration du bandwidth manager

echo "  TC par IP: ${DOWN_PAR_IP}kbit down, ${UP_PAR_IP}kbit up"
. /usr/local/bin/tc-ip.sh    # limitations individuelles



echo "  TC globales"
. /usr/local/bin/tc-rules.sh # charger la definition des classes tc



# Traffic par defaut
echo "==== Classification en low prio par defaut ===="
${IPT} -t mangle -A FORWARD -p tcp -d ${LAN_RANGE} -j CLASSIFY --set-class 1:0080
${IPT} -t mangle -A FORWARD -p udp -d ${LAN_RANGE} -j CLASSIFY --set-class 1:0080
${IPT} -t mangle -A FORWARD -p tcp -s ${LAN_RANGE} -j CLASSIFY --set-class 1:0080
${IPT} -t mangle -A FORWARD -p udp -s ${LAN_RANGE} -j CLASSIFY --set-class 1:0080


echo "  IPT, classification par IP"
. /usr/local/bin/ipt-ip.sh   # classification individuelle

echo "  Les LowPrio: ${LAN_LAMBDA_NOLIMIT_NAME}"




for LNL in ${LAN_LAMBDA_NOLIMIT}
do
    ${IPT} -t mangle -A FORWARD -p tcp -d ${LNL}/32  -j CLASSIFY --set-class 1:0080
    ${IPT} -t mangle -A FORWARD -p udp -d ${LNL}/32  -j CLASSIFY --set-class 1:0080
    ${IPT} -t mangle -A FORWARD -p tcp -s ${LNL}/32  -j CLASSIFY --set-class 1:0080
    ${IPT} -t mangle -A FORWARD -p udp -s ${LNL}/32  -j CLASSIFY --set-class 1:0080
done



for P in ${PORTS_TRAVAIL} # Mail IMAPs, SMTPs
do
    ${IPT} -t mangle -A FORWARD -p tcp --sport ${P} -d ${LAN_RANGE}  -j CLASSIFY --set-class 1:0060
    ${IPT} -t mangle -A FORWARD -p tcp --dport ${P} -s ${LAN_RANGE}  -j CLASSIFY --set-class 1:0060
done


for GGL in $(< /usr/local/bin/google.txt)  # SRV Google
do
    ${IPT} -t mangle -A FORWARD -p tcp  --dport 443 -d ${GGL} -s ${LAN_RANGE}  -j CLASSIFY --set-class 1:0060
    ${IPT} -t mangle -A FORWARD -p tcp  --sport 443 -s ${GGL} -d ${LAN_RANGE}  -j CLASSIFY --set-class 1:0060
done


for P in ${PORTS_LIVRAISON}
do
    ${IPT} -t mangle -A FORWARD -p tcp --sport ${P} -d ${LAN_RANGE}  -j CLASSIFY --set-class 1:0050
    ${IPT} -t mangle -A FORWARD -p tcp --dport ${P} -s ${LAN_RANGE}  -j CLASSIFY --set-class 1:0050
done



for SRV in $(< /usr/local/bin/servers.txt) # ${SERVEURS_TRAVAIL}
do
    ${IPT} -t mangle -A FORWARD -p tcp  -d ${SRV} -s ${LAN_RANGE}  -j CLASSIFY --set-class 1:0030
    ${IPT} -t mangle -A FORWARD -p tcp  -s ${SRV} -d ${LAN_RANGE}  -j CLASSIFY --set-class 1:0030
    
    ${IPT} -t mangle -A FORWARD -p udp  -d ${SRV} -s ${LAN_RANGE}  -j CLASSIFY --set-class 1:0030
    ${IPT} -t mangle -A FORWARD -p udp  -s ${SRV} -d ${LAN_RANGE}  -j CLASSIFY --set-class 1:0030

done

echo "  Les VIP: ${LAN_VIP_USERS_NAME}"

for VIP in ${LAN_VIP_USERS}
do
    ${IPT} -t mangle -A FORWARD -p tcp -d  ${VIP}/32  -j CLASSIFY --set-class 1:0020
    ${IPT} -t mangle -A FORWARD -p tcp -s  ${VIP}/32  -j CLASSIFY --set-class 1:0020
    ${IPT} -t mangle -A FORWARD -p udp -d  ${VIP}/32  -j CLASSIFY --set-class 1:0020
    ${IPT} -t mangle -A FORWARD -p udp -s  ${VIP}/32  -j CLASSIFY --set-class 1:0020
done

for IP_PUTEAUX in 84.55.146.194/32 82.124.29.246/32
do 
    ${IPT} -t mangle -A FORWARD -p udp -d ${IP_PUTEAUX} -s ${LAN_RANGE}  -j CLASSIFY --set-class 1:0020
    ${IPT} -t mangle -A FORWARD -p udp -s ${IP_PUTEAUX} -d ${LAN_RANGE}  -j CLASSIFY --set-class 1:0020
    
    ${IPT} -t mangle -A FORWARD -p tcp -d ${IP_PUTEAUX} -s ${LAN_RANGE}  -j CLASSIFY --set-class 1:0020
    ${IPT} -t mangle -A FORWARD -p tcp -s ${IP_PUTEAUX} -d ${LAN_RANGE}  -j CLASSIFY --set-class 1:0020
done

echo "  La ToIP: ${TOIP}"

for IP_TOIP in ${TOIP}
do
    ${IPT} -t mangle -A FORWARD -p udp -d ${IP_TOIP}  -j CLASSIFY --set-class 1:0040
    ${IPT} -t mangle -A FORWARD -p udp -s ${IP_TOIP}  -j CLASSIFY --set-class 1:0040    

    ${IPT} -t mangle -A FORWARD -p tcp -d ${IP_TOIP}  -j CLASSIFY --set-class 1:0040
    ${IPT} -t mangle -A FORWARD -p tcp -s ${IP_TOIP}  -j CLASSIFY --set-class 1:0040    

done

${IPT} -t mangle -A FORWARD -p udp --dport 6666 -d ${LAN_RANGE}  -j CLASSIFY --set-class 1:0010
${IPT} -t mangle -A FORWARD -p udp --sport 6666 -s ${LAN_RANGE}  -j CLASSIFY --set-class 1:0010

${IPT} -t mangle -A FORWARD -p icmp -d ${LAN_RANGE}  -j CLASSIFY --set-class 1:0010
${IPT} -t mangle -A FORWARD -p icmp -s ${LAN_RANGE}  -j CLASSIFY --set-class 1:0010



${IPT} -t mangle -A OUTPUT -o eth1 -j CLASSIFY --set-class 1:0010
${IPT} -t mangle -A OUTPUT -o eth0 -j CLASSIFY --set-class 1:0010





