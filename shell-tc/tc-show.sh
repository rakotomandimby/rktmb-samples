#!/bin/bash

tc -s -d class show dev eth0 | grep -A 5 'htb 1:1 '

echo ""

tc -s -d class show dev eth1 \
    | grep -A 5 ':70' \
    | awk '/^ rate/ {if (match($2, "Kbit")) {sub("Kbit", "", $2); $2=$2*1024} else {sub("bit", "", $2)} print "LOW upload\t\t\t",$2;}'
tc -s -d class show dev eth0 \
    | grep -A 5 ':70' \
    | awk '/^ rate/ {if (match($2, "Kbit")) {sub("Kbit", "", $2); $2=$2*1024} else {sub("bit", "", $2)} print "LOW download\t\t\t",$2;}'

echo ""

tc -s -d class show dev eth1 \
    | grep -A 5 ':50' \
    | awk '/^ rate/ {if (match($2, "Kbit")) {sub("Kbit", "", $2); $2=$2*1024} else {sub("bit", "", $2)} print "VIP upload\t\t\t",$2;}'
tc -s -d class show dev eth0 \
    | grep -A 5 ':50' \
    | awk '/^ rate/ {if (match($2, "Kbit")) {sub("Kbit", "", $2); $2=$2*1024} else {sub("bit", "", $2)} print "VIP download\t\t\t",$2;}'

echo ""

tc -s -d class show dev eth1 \
    | grep -A 5 ':40' \
    | awk '/^ rate/ {if (match($2, "Kbit")) {sub("Kbit", "", $2); $2=$2*1024} else {sub("bit", "", $2)} print "Email and Google upload\t\t",$2;}'
tc -s -d class show dev eth0 \
    | grep -A 5 ':40' \
    | awk '/^ rate/ {if (match($2, "Kbit")) {sub("Kbit", "", $2); $2=$2*1024} else {sub("bit", "", $2)} print "Email and Google download\t",$2;}'

echo ""

tc -s -d class show dev eth1 \
    | grep -A 5 ':30' \
    | awk '/^ rate/ {if (match($2, "Kbit")) {sub("Kbit", "", $2); $2=$2*1024} else {sub("bit", "", $2)} print "Wiki, Srv and VPN upload\t",$2;}'
tc -s -d class show dev eth0 \
    | grep -A 5 ':30' \
    | awk '/^ rate/ {if (match($2, "Kbit")) {sub("Kbit", "", $2); $2=$2*1024} else {sub("bit", "", $2)} print "Wiki, Srv and VPN download\t",$2;}'

echo ""

tc -s -d class show dev eth1 \
    | grep -A 5 ':20' \
    | awk '/^ rate/ {if (match($2, "Kbit")) {sub("Kbit", "", $2); $2=$2*1024} else {sub("bit", "", $2)} print "SSH upload\t\t\t",$2;}'
tc -s -d class show dev eth0 \
    | grep -A 5 ':20' \
    | awk '/^ rate/ {if (match($2, "Kbit")) {sub("Kbit", "", $2); $2=$2*1024} else {sub("bit", "", $2)} print "SSH download\t\t\t",$2;}'

echo ""

tc -s -d class show dev eth1 \
    | grep -A 5 ':10' \
    | awk '/^ rate/ {if (match($2, "Kbit")) {sub("Kbit", "", $2); $2=$2*1024} else {sub("bit", "", $2)} print "Skype upload\t\t\t",$2;}'
tc -s -d class show dev eth0 \
    | grep -A 5 ':10' \
    | awk '/^ rate/ {if (match($2, "Kbit")) {sub("Kbit", "", $2); $2=$2*1024} else {sub("bit", "", $2)} print "Skype download\t\t\t",$2;}'

