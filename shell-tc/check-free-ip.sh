#!/bin/bash

export USERFILE="/etc/sysconfig/users.txt"

echo "|| IP || Nom ||"

for IP in $(prips 192.168.0.0/24)
do
    if (grep -E "${IP}$" ${USERFILE} > /dev/null)
    then 
        awk -F ',' -v ip=${IP} '(ip==$4){print "|", ip, "|", $2, "|";}' < ${USERFILE}
    else
        echo "| ${IP} |  |" 
    fi
done

