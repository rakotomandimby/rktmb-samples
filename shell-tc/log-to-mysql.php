<?php

gc_enable();

class logFile
{
  public static $interesting_domains=array('wikipedia.org',
                                           'calendar.google.com',
                                           'apis.google.com',
                                           'googlemail.com',
                                           'ideotechnologies', 
                                           'ideomobilis' , 
                                           'ideocorp', 
                                           'ideoneov', 
                                           'tinypm', 
                                           'recette',
                                           'preprod',
                                           'neov.net', 
                                           'nfrance.com');
  public static $interesting_dhcp=array('DHCPOFFER on', 'DHCPACK on');
  public static $interesting_ssh=array('Accepted password');
  
  
  public static function is_data_array_ok($le_tableau, $taille)
  {
   // Au lieu de faire:
   //   if(isset($le_tableau[1]) 
   //    && isset($le_tableau[2]) 
   //    && isset($le_tableau[3]) 
   //    && isset($le_tableau[4]) 
   //    && isset($le_tableau[5]))
    $resultat=true;
    // depart a 1 (mais pas 0)
    for ($i=1; $i <= $taille; $i++)
      {
        $resultat=$resultat && isset($le_tableau[$i]);
      }
    return $resultat;
  
  }

  /* teste si la ligne contient l'une des elements du $pattern
   * et return true
   * return false en cas d'echec
   */
  public static function is_interesting_line($pattern,$a_line)
  {
    $result=false;

    foreach($pattern as $a_domain)
      {
        if(strstr($a_line, $a_domain))
          {$result=$result||true;}
        else
          {$result=$result||false;}
      }

    return $result; 
  }

  /* fait un split de $ligne
   * et insert dans la table DNSACTIVITY
   */
  public static function insert_domain_entry($line)
  {
    date_default_timezone_set('Indian/Antananarivo');
    $today = getdate();
    $dbh=self::connect_to_db('bwstat', '5o1VO');
    //              1       2           3           4              5               6
    preg_match("/^(\w+)\s+(\d+)\s+(\d+:\d+:\d+)\s+(\w+).+client\s+(.+)#.+query:\s+(.+)\s+IN.+$/",$line, $matches);
    $insert_query="INSERT INTO dns_activity (date, time, query, name ) VALUES (:date, :time, :query, :name)";
    $insert_statement=$dbh->prepare($insert_query);

    if(self::is_data_array_ok($matches, 6)) 
      {
        $insert_statement->bindParam('date', date('Y-m-d', strtotime($matches[2].' '.$matches[1].' ' .$today['year'])));
        $insert_statement->bindParam('time', $matches[3]);
        $insert_statement->bindParam('query',$matches[6]);
        $hostname_array=explode('.',gethostbyaddr($matches[5]));
        $insert_statement->bindParam('name', $hostname_array[0]);
        $insert_statement->execute(); 
      }   

  }

  /* fait un split de $ligne
   * et insert dans la table SSHACTIVITY
   */
  public static function insert_ssh_entry($line)
  {
    date_default_timezone_set('Indian/Antananarivo');
    $today = getdate();
    $dbh=self::connect_to_db('bwstat', '5o1VO');
        
    // Jul 2 08:12:50 dev5 sshd[6891]: Accepted password for dev5 from 192.168.0.124 port 49251 ssh2
    //              1       2           3          4                   5
    preg_match("/^(\w+)\s+(\d+)\s+(\d+:\d+:\d+)\s+(.+)\s+sshd.+from\s+(.+)\s+port.+$/",$line, $matches);
      
    $insert_query="INSERT INTO ssh_activity (date, time, server, name ) VALUES (:date, :time, :server, :name)";
    $statement=$dbh->prepare($insert_query);

    if(self::is_data_array_ok($matches, 5))
      {
        $statement->bindParam('date', date('Y-m-d', strtotime($matches[2].' '.$matches[1].' ' .$today['year'])));
        $statement->bindParam('time', $matches[3]);
        $statement->bindParam('server',$matches[4]);
        $hostname_array=explode('.',gethostbyaddr($matches[5]));
        $statement->bindParam('name', $hostname_array[0]);
        $statement->execute();    
      }
  }

  /* fait un split de $ligne
   * et insert dans la table DHCPACTIVITY
   */
  public static function insert_dhcp_entry($line)
  {
    date_default_timezone_set('Indian/Antananarivo');
    $today = getdate();
    $dbh=self::connect_to_db('bwstat', '5o1VO');

    // Jul  2 08:12:10 gateway dhcpd: DHCPOFFER on 192.168.0.127 to 00:1b:b9:64:77:a8 via eth0
    // Jul  2 08:13:58 gateway dhcpd: DHCPACK to 192.168.0.145 (10:78:d2:2f:5a:69) via eth0
    // Jul  2 03:26:26 gateway dhcpd: DHCPACK on 192.168.0.233 to 00:25:11:40:b6:05 via eth0
    //              1       2           3                   4                   5
    preg_match("/^(\w+)\s+(\d+)\s+(\d+:\d+:\d+).+dhcpd:\s+(\w+)\s+on\s+(\d+\.\d+\.\d+\.\d+).+$/",$line, $matches);
    $insert_query="INSERT INTO dhcp_activity (date, time, dhcp, name ) VALUES (:date, :time, :dhcp, :name)";
    $statement=$dbh->prepare($insert_query);
    
    if(self::is_data_array_ok($matches, 5))
      {
        $statement->bindParam('date', date('Y-m-d', strtotime($matches[2].' '.$matches[1].' ' .$today['year'])));
        $statement->bindParam('time', $matches[3]);
        $statement->bindParam('dhcp',$matches[4]);
        $hostname_array=explode('.',gethostbyaddr($matches[5]));
        $statement->bindParam('name', $hostname_array[0]);
        $statement->execute(); 
      }    
  }

  public static function logInsert($filename)
  {
    $array_of_file=file($filename);
    
    foreach ($array_of_file as $a_line)
      {
        if(self::is_interesting_line(self::$interesting_domains, $a_line))
          {
            self::insert_domain_entry($a_line);
          }
        elseif(self::is_interesting_line(self::$interesting_ssh, $a_line))
          {
            self::insert_ssh_entry($a_line);
          }
        elseif(self::is_interesting_line(self::$interesting_dhcp, $a_line))
          {
            self::insert_dhcp_entry($a_line);
          }
      }
    
  }

  private static function connect_to_db($user, $pass)
  {
    try
      {
        $dbh = new PDO('mysql:host=localhost;dbname=backteam_bwstat', $user, $pass);
      }
    catch (Exception $e) 
      {
        print "Erreur database!: " . $e->getMessage();
      }
    return $dbh; 
  }
  
}


logFile::logInsert('/tmp/messages');
gc_disable();

?>

