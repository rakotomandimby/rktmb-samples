#!/bin/bash


case $1 in
    up )
        for IP_DU_LAN in $(awk '{print $4}' < /tmp/users)
        do
            
            IP_UNDERSCORE=$(echo ${IP_DU_LAN} | sed 's/\./_/g')
            CHAIN_IN="acct_in_"${IP_UNDERSCORE}
            CHAIN_OUT="acct_out_"${IP_UNDERSCORE}
            
            ${IPT} -t filter -N ${CHAIN_IN}
            ${IPT} -t filter -I FORWARD -o ${IFLAN} -d ${IP_DU_LAN} -j ${CHAIN_IN}
            ${IPT} -t filter -A ${CHAIN_IN} -j RETURN
            
            ${IPT} -t filter -N ${CHAIN_OUT}
            ${IPT} -t filter -I FORWARD -o ${IFWAN} -s ${IP_DU_LAN} -j ${CHAIN_OUT}
            ${IPT} -t filter -A ${CHAIN_OUT} -j RETURN
        done
        ;;
    down )
        for IP_DU_LAN in $(${PRIPS} ${LAN_RANGE})
        do
            
            IP_UNDERSCORE=$(echo ${IP_DU_LAN} | sed 's/\./_/g')
            CHAIN_IN="acct_in_"${IP_UNDERSCORE}
            CHAIN_OUT="acct_out_"${IP_UNDERSCORE}
            
            ${IPT} -t filter -F ${CHAIN_IN}  &> /var/log/messages # Attention, redirection systematique vers les logs  
            ${IPT} -t filter -X ${CHAIN_IN}  &> /var/log/messages # Attention, redirection systematique vers les logs
            
            ${IPT} -t filter -F ${CHAIN_OUT} &> /var/log/messages # Attention, redirection systematique vers les logs
            ${IPT} -t filter -X ${CHAIN_OUT} &> /var/log/messages # Attention, redirection systematique vers les logs  
        done
        ;;
esac

