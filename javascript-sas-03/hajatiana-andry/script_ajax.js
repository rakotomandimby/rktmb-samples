﻿//création du grand tableau
//création de son entête(objet tableau)
//création de son corps(objet tableau)
var leTableau = document.createElement("table");
var click_xhr = 0;
leTableau.listeDesLignes = new Array();
leTableau.listeDesHead = new Array();
//creation de l'entête
leTableau.theadDuTableau = leTableau.appendChild(document.createElement('thead'));
//creation du corps
leTableau.tbodyDuTableau = leTableau.appendChild(document.createElement('tbody'));

//création de la ligne d'entête
leTableau.arrayToTh = function (un_th) {
    var le_thead = document.createElement("tr");
    for (var col = 0; col < un_th.length; col ++ ){
        var texte = document.createTextNode(un_th[col]);
        var le_th = document.createElement("th");
        var sort_asc = document.createElement("img");
        var sort_desc = document.createElement("img");
        le_th.appendChild(texte);
        sort_asc.setAttribute('src','images/sort_asc.png');
        sort_asc.setAttribute('onclick', 'leTableau.sortTableAsc('+col+');');
        le_th.appendChild(sort_asc);
        sort_desc.setAttribute('src','images/sort_desc.png');
        sort_desc.setAttribute('onclick', 'leTableau.sortTableDesc('+col+');');
        le_th.appendChild(sort_desc);
        le_thead.appendChild(le_th);
    }
    le_thead.style.backgroundColor = '#616263';
    return le_thead;
}

//Ajout de la ligne d'entête
leTableau.appendHead = function (){
    var un_head = leTableau.arrayToTh(leTableau.listeDesHead); 
    leTableau.theadDuTableau.appendChild(un_head);
}

//Création d'une ligne de tableau
leTableau.arrayToTR = function(un_tableau) {
    var le_tr = document.createElement("tr");
    for (var colonne = 0; colonne < un_tableau.length; colonne ++ ){
        var texte = document.createTextNode(un_tableau[colonne]);
        var un_td = document.createElement("td");
        un_td.appendChild(texte);
        le_tr.appendChild(un_td);
    }
    return le_tr;
};

//Affichage de toutes les lignes
leTableau.appendAllLines = function() {
    for (var ligne = 0; ligne < leTableau.listeDesLignes.length; ligne++){
        var la_ligne = leTableau.listeDesLignes[ligne]
        var ligne_convertie = leTableau.arrayToTR(la_ligne);
        leTableau.tbodyDuTableau.appendChild(ligne_convertie);
        // changement de couleur si la position de la ligne est divisible par 2 
        var compteur = ligne%2;
        if (compteur == 0){
        //changement de couleur en gris de la ligne
            leTableau.tbodyDuTableau.lastChild.style.backgroundColor = '#b3b0b0';
        }else{
        //changement de couleur en bleu de la ligne
            leTableau.tbodyDuTableau.lastChild.style.backgroundColor = '#c7c7ca';
        }
    }
}

//xhr 
leTableau.setLeXML = function (an_xml) {
    leTableau.leXML=an_xml;
}

leTableau.getLeXML = function () {
    return leTableau.leXML;
}

leTableau.getColumns = function () {
    // on prend le leXML
    var mon_xml = leTableau.getLeXML();
    // on cherche les tag 'centre'
    var ma_liste_des_centres = mon_xml.getElementsByTagName('centre');
    // on prend le premier
    var un_centre = ma_liste_des_centres[0];
    // on liste ses enfants -> colonnes
    for ( var enfant = 0; enfant < un_centre.children.length; enfant ++) {
        var mon_enfant = un_centre.children[enfant].nodeName;
        leTableau.listeDesHead.push(mon_enfant);
    }
}

leTableau.isMonEnfantVide = function (une_cellule) {
    if (une_cellule.firstChild == null) {return true;}
    else                                {return false;}
}

leTableau.remplirEnfant = function (un_node) {
    var texte_remplissage = document.createTextNode(" ");
    un_node.appendChild(texte_remplissage);
}

//Récupération des valeurs du XML
leTableau.getData = function () {
    // on prend le leXML
    var mon_xml = leTableau.getLeXML();
    // on cherche les tag 'centre'
    var ma_liste_des_centres = mon_xml.getElementsByTagName('centre');
    // on les prends un par un 
    for ( var rang_centre = 0; rang_centre < ma_liste_des_centres.length; rang_centre ++) {
	var mes_colonnes = new Array(); 
	var un_centre = ma_liste_des_centres[rang_centre];
	// on liste ses enfants -> colonnes
	for ( var enfant = 0; enfant < un_centre.children.length; enfant ++) {
	    var node_enfant = un_centre.children[enfant];
	    if( leTableau.isMonEnfantVide(node_enfant)){
		leTableau.remplirEnfant(node_enfant);
	    }
	    var mon_enfant = un_centre.children[enfant].firstChild.data;
	    
	    mes_colonnes.push(mon_enfant);
	}
    leTableau.listeDesLignes.push(mes_colonnes);
    }
}


//Verification d'etat et reponse du xhr
leTableau.onReadyState = function() {
    // This is used by the below funtion, have to declare it before.
    // No more
    if (xhr.readyState == 4 && click_xhr == 0 && (xhr.status == 200 || xhr.status == 0)) {
	leTableau.setLeXML(xhr.responseXML); // <====== here is the point!
	leTableau.getColumns();
	leTableau.appendHead();
	leTableau.getData();
	leTableau.appendAllLines();
	leConteneur.appendChild(leTableau);
    click_xhr = 1;
    }
}
//requete XHR
leTableau.getTheXML = function() {
    xhr = new XMLHttpRequest();
    // the XHR has to be a global thing
    xhr.onreadystatechange = leTableau.onReadyState;
    xhr.onprogress= barprog;
    xhr.open("GET", "../data.xml", true);
    xhr.send(null);
    // From here, this.leXML stores the XML from data.xml
}

function barprog(pos){
        var outsidebar;
        var insidebar;
        /*
            Récuperation des 2 id du div interieur et exterieur
        */
        outsidebar = document.getElementById('outsidebar');
        insidebar = document.getElementById('insidebar');
        /*
            calcul en pourcentage de la barre de progression
        */
        var percent = (pos.position / pos.totalSize)*100;	
        /*
            Dynamisation de la barre de progression interne
        */            
		insidebar.style.width=(250*Math.round(percent))/100 +"px";
        
        //style pour afficher les progressions
		outsidebar.style.display='block'; 
        insidebar.style.display='block';
        /*
            Tester si la progression est finie
            si oui ne plus afficher la barre de progression
        */
        if(percent==100){
            outsidebar.style.display='none'; 
            insidebar.style.display='none'; 
        }
    }
    
//Convertion de date    
function charDate(charset){
    var reg_date = new RegExp("[0-9]{2}[/]{1}[0-9]{2}[/]{1}[0-9]{2,4}","g");
    if(reg_date.test(charset) == true){ 
        return true;
    }else{
        return false;
    }
}

//fonction  
function testNull(col_pos){
    for(var row = 0; row< leTableau.listeDesLignes.length; row++){
		any_charset=leTableau.listeDesLignes[row][col_pos];
		if(any_charset != " ")
		{		
			return (any_charset);			
			break;
		}
	}
}

//fonction convertion de date  
function sortDatedesc(date){
    var dt = " ";
    var europeandate = true;
    if (date.length == 10) 
    {
		if (europeandate == false) 
        {
			dt = (date.substr(6,4)+date.substr(0,2)+date.substr(3,2));
			return dt;
		} else 
        {
			dt = date.substr(6,4)+date.substr(3,2)+date.substr(0,2);
			return dt;
		}
	} 
    else if (date.length == 8) 
    {
		yr = date.substr(6,2);
		if (parseInt(yr) < 50) 
        { 
			yr = '20'+yr; 
		} else { 
			yr = '19'+yr; 
		}
		if (europeandate == true) 
        {
			dt = yr+date.substr(3,2)+date.substr(0,2);
			return dt;
		} else {
			dt = yr+date.substr(0,2)+date.substr(3,2);
			return dt;
		}
	}
	return dt;
}
function sortDate(date){
    var dt = "~"; 
    var europeandate = true;
    if (date.length == 10) 
    {
		if (europeandate == false) 
        {
			dt = (date.substr(6,4)+date.substr(0,2)+date.substr(3,2));
			return dt;
		} else 
        {
			dt = date.substr(6,4)+date.substr(3,2)+date.substr(0,2);
			return dt;
		}
	} 
    else if (date.length == 8) 
    {
		yr = date.substr(6,2);
		if (parseInt(yr) < 50) 
        { 
			yr = '20'+yr; 
		} else { 
			yr = '19'+yr; 
		}
		if (europeandate == true) 
        {
			dt = yr+date.substr(3,2)+date.substr(0,2);
			return dt;
		} else {
			dt = yr+date.substr(0,2)+date.substr(3,2);
			return dt;
		}
	}
	return dt;
}
//fonction de tri ascendant
function triDateAsc(col_a,col_b){
    return (sortDate(col_a[any_col]) < sortDate(col_b[any_col]))? -1 : 1;
}
function triDefaultAsc(col_a,col_b)
{
	return (convertStringValueasc(col_a[any_col].toLowerCase(),0) < convertStringValueasc(col_b[any_col].toLowerCase(),0))? -1 : 1;
}

//function de tri descendant
function triDateDesc(col_a,col_b){
    if(col_a[any_col] !="" && col_b[any_col] !=""){
    return (sortDatedesc(col_a[any_col]) > sortDatedesc(col_b[any_col]))? -1 : 1;
    }else{
    return (sortDate() < sortDate(col_b[any_col]))? -1 : 1;
    }
}
function triDefaultDesc(col_a,col_b)
{
	return (convertStringValue(col_a[any_col].toLowerCase(),0) > convertStringValue(col_b[any_col].toLowerCase(),0))? -1 : 1;
}

//converstion des chaines de caractère en une chaîne sans caractère
function convertStringValue(charset, index)
{
	if(charset != " ")
	{
		return(sansAccent(charset));
	}
	else
	{
		if(index == 0)
		{
			charset = " ";
			return(charset);
		}
		else
		{
			charset ="~";
			return(charset);
		}
	}
}

//Fonction teste de vide
function convertStringValueasc(charset, index)
{
	if(charset != " ")
	{
		return(sansAccent(charset));
	}
	else
	{
		if(index == 0)
		{
			charset = "~";
			return(charset);
		}
		else
		{
			charset =" ";
			return(charset);
		}
	}
}
//transformation de tous les lettres avec accents pour être sans accent dans une chaîne de caractères
function sansAccent(charset) 
{
	var acc = new Array('À','Á','Â','Ã','Ä','Å','Æ','Ç','È','É','Ê','Ë','Ì','Í','Î','Ï', 'Ð','Ñ','Ò','Ó','Ô','Õ','Ö','Ø','Ù','Ú','Û','Ü','Ý','Þ','ß', 'à','á','â','ã','ä','å','æ','ç','è','é','ê','ë','ì','í','î','ï','ð','ñ', 'ò','ó','ô','õ','ö','ø','ù','ú','û','ü','ý','ý','þ','ÿ');
	var no_acc = new Array('A','A','A','A','A','A','A','C','E','E','E','E','I','I','I','I', 'D','N','O','O','O','0','O','O','U','U','U','U','Y','b','s', 'a','a','a','a','a','a','a','c','e','e','e','e','i','i','i','i','d','n', 'o','o','o','o','o','o','u','u','u','u','y','y','b','y');
	for (var i = 0; i < no_acc.length; i++)
	charset = replaceAll(charset, acc[i], no_acc[i]);
	return charset;
 }
 //Remplacement de tous les caractères avec accent en sans accent 
function replaceAll(charset, acc, no_acc)
{
	while (charset.indexOf(acc) != -1)
	charset = charset.replace(acc, no_acc);
	return charset;
 }
 //Création et Insertion du corps de tableau après triage
leTableau.appendAllBodyLines = function() 
{
    var le_tbody = document.createElement("tbody");
	
    for (var ligne = 0; ligne < leTableau.listeDesLignes.length; ligne++)
	{
				var la_ligne = leTableau.listeDesLignes[ligne]
				var ligne_convertie = leTableau.arrayToTR(la_ligne);
				le_tbody.appendChild(ligne_convertie);
    }
	leTableau.appendChild(le_tbody);
}
//Affectation de style après avoir trié
leTableau.affecterCSS = function (nomNoeud)
{
	var le_tr = document.getElementsByTagName(nomNoeud);
	for ( var compteur = 1; compteur < le_tr.length; compteur++)
	{
		if(compteur%2==0)
		{
			le_tr[compteur].style.backgroundColor = '#c7c7ca';
		}
		else
		{
			le_tr[compteur].style.backgroundColor = '#b3b0b0';
		}
	}	
};

//test et affichage des lignes triées croissantes 
leTableau.sortTableAsc = function(col_pos) {
	any_col = col_pos;
	leTableau.removeChild(leTableau.lastChild);
	testNull(col_pos);
	if (charDate(any_charset) == true)//si c'est une date
	{
		leTableau.listeDesLignes.sort(triDateAsc);
		leTableau.appendAllBodyLines();
		leTableau.affecterCSS("tr");
	}
	else { //sinon considéré comme chaîne de caractère
		leTableau.listeDesLignes.sort(triDefaultAsc);
		leTableau.appendAllBodyLines();
		leTableau.affecterCSS("tr");
					
		}
}
//test et affichage des lignes triées décroissantes
leTableau.sortTableDesc = function(col_pos) {
	any_col = col_pos;
	leTableau.removeChild(leTableau.lastChild);
	testNull(col_pos);
	if (charDate(any_charset) == true)
	{
		leTableau.listeDesLignes.sort(triDateDesc);//tri de la colonne date
		leTableau.appendAllBodyLines();//Affichage du corps du tableau
		leTableau.affecterCSS("tr");//style css
	}
	else { 
	    leTableau.listeDesLignes.sort(triDefaultDesc);//tri de la colonne des chaînes de caractère
	    leTableau.appendAllBodyLines();
	    leTableau.affecterCSS("tr");
					
	}
        
   
}
//fonction principale pour l'affichage
function afficher()
{
    leConteneur =  document.getElementById('contenu');
    leTableau.getTheXML();
}

