
var leTableau = document.createElement("table");
leTableau.listeDesLignes = new Array();
// utlise dans l entette pour bloquer l image 
var cpt_img;
// pour bloquer les l ajout du tableau a chaque clic du bouton
var cpt_xhr = 0;
// pour les decaler les couleurs
var k =0;
//var index_colonne;

//Barre de progression
leTableau.progression = function(e){
    var barre = document.getElementById('barre'); 
    var cadre = document.getElementById('cadre');  
    var wCadre = parseInt(cadre.clientWidth);    
    var pourcentage = (e.position / e.totalSize)*100;
    barre.style.width= (pourcentage*wCadre)/100+'px'; //dynamisation de la longeur de la barre de progression
    
    cadre.style.display = 'block';
    barre.style.display = 'block';  
    if(pourcentage == 100){
        barre.style.visibility = 'Hidden';
        cadre.style.visibility = 'Hidden';
    }
}

leTableau.arrayToTR = function(un_tableau) {
    var le_tr = document.createElement("tr");
    for (var colonne = 0; colonne < un_tableau.length; colonne ++ ){
		var texte = document.createTextNode(un_tableau[colonne]);
		var un_td = document.createElement("td");
		un_td.setAttribute("class","colonne_"+colonne);	
		//sort_column_class = un_td.className;
		//un_td.setAttribute("onclick","leTableau.triParClasse(this.className)");	//+colonne+",leTableau.triAlphabetique)"); 
		un_td.appendChild(texte);
		if (cpt_img < un_tableau.length){
		// ajout image 
			var image_croissant = document.createElement("img");
			var image_decroissant = document.createElement("img");
			image_croissant.setAttribute("src", "/projets/projet-javascript-sas/Mandimby-Claire/Projet final/images/triangle-asc.gif");
			image_croissant.setAttribute("onclick","leTableau.triCroissantAveccolonne("+colonne+")");
			image_decroissant.setAttribute("src", "/projets/projet-javascript-sas/Mandimby-Claire/Projet final/images/triangle-desc.gif");
			image_decroissant.setAttribute("onclick","leTableau.triDecroissantAveccolonne("+colonne+")");
			un_td.appendChild(document.createTextNode("  "));
			un_td.appendChild(image_croissant);
			un_td.appendChild(document.createTextNode("  "));
			un_td.appendChild(image_decroissant);
			le_tr.setAttribute("class","en_tete");
			cpt_img ++;
		}
		le_tr.appendChild(un_td);		
    }	
    return le_tr;	
};

//changement couleur
leTableau.changeColor = function(ligne){
	k++;
        if(k%2 == 0)
            ligne.className = "couleur1";
		else
			ligne.className = "couleur2";
}

// Tri sur une colonne
leTableau.triCroissantAveccolonne = function(numero_colonne){	
	// supprimer la dernere ligne du tableau
	//leTableau.removeChild(leTableau.lastChild);
	// tester si le contenu est vide
	var le_contenu = getContenu(numero_colonne);
	// recuperer les lignes
	var lignes = leTableau.getElementsByTagName("tr");
	// creer un tableau pour stocker les valeur trier
	var trier = new Array();
	trier.length = 0;
	// trier les td de chaque colonne
	var i = 0;
	while(lignes[++i]){		
	
		trier.push([lignes[i],lignes[i].getElementsByTagName('td')[numero_colonne].firstChild.nodeValue])
	}
	
	//tri Date	
	if(isDate(le_contenu) == true){	
		trier.sort(leTableau.triDateAsc);
	}	
	else{
		//tri alphabetique
		if(isText(le_contenu) == true){	
			trier.sort(leTableau.triAlphabetiqueAsc);	
		}
	}	
		
	var j = 0;	
	k = 0;
	while(trier[++j]){
		leTableau.changeColor(trier[j][0]);
		leTableau.appendChild(trier[j][0]);		
	}	

}

leTableau.triDecroissantAveccolonne = function(numero_colonne){
	// supprimer la dernere ligne du tableau
	leTableau.removeChild(leTableau.lastChild);
	// tester si le contenu est vide
	var le_contenu = getContenu(numero_colonne);
	// recuperer les lignes
	var lignes = leTableau.getElementsByTagName("tr");
	// creer un tableau pour stocker les valeur trier
	var trier = new Array();
	trier.length = 0;
	// trier les td de chaque colonne
	var i = 0;
	while(lignes[++i]){		
		trier.push([lignes[i],lignes[i].getElementsByTagName('td')[numero_colonne].firstChild.nodeValue])
	}
	if ((numero_colonne == 0) || (numero_colonne == 1)){
		//if(isDate(le_contenu) == true)
			trier.sort(leTableau.triDateDesc);
			console.log("date");
	}
	else{
		trier.sort(leTableau.triAlphabetiqueDesc);	
	}
	//tri alphabetique
	/*if(isText(le_contenu) == true)
		trier.sort(leTableau.triAlphabetiqueDesc);	
	*/
	var j = 0;
	k = 0;	
	while(trier[++j]){		
		leTableau.changeColor(trier[j][0]);
		leTableau.appendChild(trier[j][0]);		
	}	
	
}

//fonction pour tester un texte
function getContenu (colonne_index){	
//colon = colonne_index;
	for(var ligne = 1; ligne< leTableau.listeDesLignes.length; ligne++){
		var le_texte =leTableau.listeDesLignes[ligne][colonne_index];
		if(le_texte != "")
			return le_texte;		
	}
}
//fonction pour tester un texte
function isText (le_texte){
	//if(le_texte != "zzz"){
		if(isNaN(le_texte))	//NAN: nombre non valide
			return true;	//texte
		else
			return false;	//numerique	
}
//fonction pour tester une Date
function isDate(la_date){
	var reg =new RegExp("^[0-9]{2}[/]{1}[0-9]{2}[/]{1}[0-9]{2}$","g");
	var bool = reg.test(la_date);
	return bool;
}
// transformer les dates pour etre utilisees dans les tris
function translateDateToInt(strDate){	
	var date = strDate.toString().split('/'); 
	var jours = date[0].split(','); 
	var jour = jours[1];
	var mois = date[1];
	var annee = date[2];
	return dateInt =  annee + mois + jour;	
}

leTableau.triDateAsc = function(a, b){		
	aa = translateDateToInt(a);	
	bb = translateDateToInt(b);		
	if(aa > bb)
		return 1
	if(aa < bb)
		return -1
	return 0		
}

leTableau.triDateDesc = function(a, b){
	if (a == null )
		a = "00/00/00";
	if (b == null )
		b = "00/00/00";
	aa = translateDateToInt(a);	
	bb = translateDateToInt(b);	
	if(aa >bb)
		return -1
	if(aa < bb)
		return 1
	return 0	
}

leTableau.triAlphabetiqueAsc = function(a,b) {	
	a=a[1].toLowerCase();
	b=b[1].toLowerCase();
	if (a != "" && b !=""){
		if(a > b)
			return 1
		if(a < b)
			return -1
		return 0
	}
	else{
		if (a == "" && b ==""){
			a="zzz";
			b="zzz";
			leTableau.triAlphabetiqueAsc(a,b); 
		}
		else{
			if (a == "" ){
				a="zzz";
				leTableau.triAlphabetiqueAsc(a,b); 
			}		
			else{
				b="zzz";
				leTableau.triAlphabetiqueAsc(a,b);  
			}
		}
	}
};

leTableau.triAlphabetiqueDesc = function(a,b) {
	a=a[1].toLowerCase();
    b=b[1].toLowerCase();
	if(a > b)
		return -1
	if(a < b)
		return 1
	return 0   
};

leTableau.appendAllLines = function() {
    for (var ligne = 0; ligne < leTableau.listeDesLignes.length; ligne++){
		var la_ligne = leTableau.listeDesLignes[ligne];
		var ligne_convertie = leTableau.arrayToTR(la_ligne);
		/*if (la_ligne == 0)
			leTableau.appendChild(ligne_convertie);		
		if (la_ligne != 0){*/
			leTableau.changeColor(ligne_convertie);
			leTableau.appendChild(ligne_convertie);
		}
}

leTableau.setLeXML = function (an_xml) {
    leTableau.leXML=an_xml;
}

leTableau.getLeXML = function () {
    return leTableau.leXML;
}

leTableau.getColumns = function () {
    cpt_img = 0;
	var mes_colonnes = new Array(); 
    // on prend le leXML
    var mon_xml = leTableau.getLeXML();
    // on cherche les tag 'centre'
    var ma_liste_des_centres = mon_xml.getElementsByTagName('centre');
    // on prend le premier
    var un_centre = ma_liste_des_centres[0];
    // on liste ses enfants -> colonnes
    for ( var enfant = 0; enfant < un_centre.children.length; enfant ++) {
		var mon_enfant = un_centre.children[enfant].nodeName;
		mes_colonnes.push(mon_enfant);
    }
    leTableau.listeDesLignes.push(mes_colonnes);
}

leTableau.isMonEnfantVide = function (une_cellule) {
    if (une_cellule.firstChild == null) {return true;}
    else                                {return false;}
}

leTableau.remplirEnfant = function (un_node) {
    var texte_remplissage = document.createTextNode("");
    un_node.appendChild(texte_remplissage);
}

leTableau.getData = function () {
	//k=1;
	// on prend le leXML
    var mon_xml = leTableau.getLeXML();
    // on cherche les tag 'centre'
    var ma_liste_des_centres = mon_xml.getElementsByTagName('centre');
    // on les prends un par un 
    for ( var rang_centre = 0; rang_centre < ma_liste_des_centres.length; rang_centre ++) {
	var mes_colonnes = new Array(); 
	var un_centre = ma_liste_des_centres[rang_centre];
	// on liste ses enfants -> colonnes
	for ( var enfant = 0; enfant < un_centre.children.length; enfant ++) {
	    var node_enfant = un_centre.children[enfant];
	    if( leTableau.isMonEnfantVide(node_enfant)){
		leTableau.remplirEnfant(node_enfant);
	    }
	    var mon_enfant = un_centre.children[enfant].firstChild.data;
	    
	    mes_colonnes.push(mon_enfant);
	}
    leTableau.listeDesLignes.push(mes_colonnes);
    }
}

leTableau.onReadyState = function() {
    // This is used by the below funtion, have to declare it before.
    // No more
    if (xhr.readyState == 4 && cpt_xhr == 0 && (xhr.status == 200 || xhr.status == 0)) {
	leTableau.setLeXML(xhr.responseXML); // <====== here is the point!
	leTableau.getColumns();
	leTableau.getData();
	leTableau.appendAllLines();
	leConteneur.appendChild(leTableau);
	cpt_xhr = 1;
    }
}

leTableau.getTheXML = function() {
    xhr = new XMLHttpRequest();
    // the XHR has to be a global thing
    xhr.onreadystatechange = leTableau.onReadyState;
	xhr.onprogress = leTableau.progression;
    xhr.open("GET", "/projets/projet-javascript-sas/data.xml", true);
    xhr.send(null);
    // From here, this.leXML stores the XML from data.xml
}

function afficher(){
    leConteneur =  document.getElementById('contenu');
    leTableau.getTheXML();
}

