var leTableau = document.createElement("table");
leTableau.listeDesLignes = new Array();
leTableau.listeDesEntete = new Array();
leTableau.enteteDuTableau = leTableau.appendChild(document.createElement('thead'));
var CHAINE_MAX = '~';
var DATE_MAX = '31/12/99';
var NBR_MAX = 99999999;
var chaine_filtres = ["","","","","","","","","","","","","","","","","","","","","","","","","","","",""];
var loading= document.getElementById("loading");


// creation de l'en-tete du tableau
leTableau.arrayToTh = function (un_entete) {
    var le_tr = document.createElement("tr");
    for (var colonne = 0; colonne < un_entete.length; colonne ++ ){
        var texte = document.createTextNode(un_entete[colonne]);
        var un_th = document.createElement("th");
        var imgUp = document.createElement("img");
        var imgDown = document.createElement("img");
        un_th.appendChild(texte);
        imgUp.setAttribute('src','images/up.png');
        imgUp.setAttribute('onclick', 'leTableau.trierColonne('+colonne+');');
        un_th.appendChild(imgUp);
        imgDown.setAttribute('src','images/down.png');
        imgDown.setAttribute('onclick', 'leTableau.inverserColonne('+colonne+');');
        un_th.appendChild(imgDown);
        le_tr.appendChild(un_th);
    }
    le_tr.style.backgroundColor = '#3a5264';
    return le_tr;
}

// creation des inputs du tableau
leTableau.inputToTr = function() {
    var un_tr = document.createElement("tr");
    for(var i=0; i<28; i++){
        var un_th = document.createElement("th");
        var un_input = document.createElement("input");
		un_input.setAttribute('placeholder',"Ajouter un filtre");
		un_input.setAttribute('onkeyup',"leTableau.filtrer();");
        un_th.appendChild(un_input); //insertion de chaque input
        un_tr.appendChild(un_th);
    }
    un_tr.style.backgroundColor = '#3a5264';
    return un_tr;
}

// ajouter l'en-tete du tableau
leTableau.appendHead = function() {
    var un_entete = leTableau.arrayToTh(leTableau.listeDesEntete);
    var les_inputs = leTableau.inputToTr();
    leTableau.enteteDuTableau.appendChild(les_inputs);
    leTableau.enteteDuTableau.appendChild(un_entete);
}

// suprimer le corps du tableau
leTableau.deleteTableBody = function(){
    // supprimer le tbody
    leTableau.removeChild(leTableau.lastChild);
} 

// tri sur une colonne
leTableau.trierColonne = function(un_num_colonne) {
    // suprimer le corps du tableau
    leTableau.deleteTableBody();
    
    // argument de tri des chaines
    function argumentTriChaine(a, b){
        if ( leTableau.maxValCellule(a[un_num_colonne].toLowerCase()) < leTableau.maxValCellule(b[un_num_colonne].toLowerCase()) ){return -1;};
        if ( leTableau.maxValCellule(a[un_num_colonne].toLowerCase()) > leTableau.maxValCellule(b[un_num_colonne].toLowerCase()) ){return 1;};
    }
    
    // argument de tri des dates
    function argumentTriDate(a, b){
        if (leTableau.changeFormatDate(leTableau.maxDate(a[un_num_colonne])) < leTableau.changeFormatDate(leTableau.maxDate(b[un_num_colonne]))){return -1;};
        if (leTableau.changeFormatDate(leTableau.maxDate(a[un_num_colonne])) > leTableau.changeFormatDate(leTableau.maxDate(b[un_num_colonne]))){return 1;};
    }
    
    // argument de tri des nombres
    function argumentTriNombre(a, b){
        parseInt(b[un_num_colonne])-parseInt(a[un_num_colonne]);
    }
    
    // la methode sort() recoit deux arguments: une valeur n�gative pour inverser les deux argument a et b et une valeur positive pour ne pas inverser.
    // choix du tri
    if( un_num_colonne == 26 ){ // tri des colonnes des nombres
        leTableau.listeDesLignes.sort(argumentTriNombre);
    }
    if ((un_num_colonne == 0)||(un_num_colonne == 1)){// colonnes des dates
        leTableau.listeDesLignes.sort(argumentTriDate);
    }else{
        leTableau.listeDesLignes.sort(argumentTriChaine);
    }
    // affiche le tableau trier
    leTableau.appendAllLines();
}

leTableau.inverserColonne = function(un_num_colonne) {
    // suprimer le corps du tableau
    leTableau.deleteTableBody();
    
    // argument de tri des chines de caract�re
    function argumentTriChaine(a, b){
        if ( a[un_num_colonne].toLowerCase() > b[un_num_colonne].toLowerCase() ){return -1;};
        if ( a[un_num_colonne].toLowerCase() < b[un_num_colonne].toLowerCase() ){return 1;};
    }
    
    // argument de tri des dates
    function argumentTriDate(a, b){
        if ( leTableau.changeFormatDate(a[un_num_colonne]) > leTableau.changeFormatDate(b[un_num_colonne]) ){return -1;};
        if ( leTableau.changeFormatDate(a[un_num_colonne]) < leTableau.changeFormatDate(b[un_num_colonne]) ){return 1;};
    }
    
    // argument de tri des nombres
    function argumentTriNombre(a, b){
        parseInt( leTableau.maxNbr( a[un_num_colonne] ) )-parseInt( leTableau.maxNbr( b[un_num_colonne] ) );
    }
    // choix du tri
    if( un_num_colonne == 26 ){ // tri des colonnes des nombres
        leTableau.listeDesLignes.sort(argumentTriNombre);
    }
    if ( (un_num_colonne == 0)||(un_num_colonne == 1) ){ // tri les colonnes des dates
        leTableau.listeDesLignes.sort(argumentTriDate);
    }else{
        leTableau.listeDesLignes.sort(argumentTriChaine);
    }
    // affiche le tableau trie
    leTableau.appendAllLines();
}

// ajout d'une valeur max aux cellules du tableau
leTableau.maxValCellule = function (une_cellule){
    if(/^\s/.test(une_cellule)){
        une_cellule = CHAINE_MAX;
    }
    return une_cellule;
}

// change le format de la date jj/mm/aa en aammjj
leTableau.changeFormatDate = function (une_date){
    if(/\s/.test(une_date)==false){
       une_date = une_date.replace(/^(\d{2})\/(\d{2})\/(\d{2})$/, '$3$2$1');
    }
    return une_date;
}

// maximiser les dates vides
leTableau.maxDate = function(une_date){
    if(/\s/.test(une_date)){
        une_date = DATE_MAX;
    }
    return une_date;
}
// maximise les colonnes des nombres vides
leTableau.maxNbr = function (un_nombre) {
    if(/^\s/.test(un_nombre)){
        un_nombre = NBR_MAX;
    }
    return un_nombre;
}

leTableau.arrayToTR = function(un_tableau) {
    var le_tr = document.createElement("tr");
    for (var colonne = 0; colonne < un_tableau.length; colonne ++ ){
        var texte = document.createTextNode(un_tableau[colonne]);
        var un_td = document.createElement("td");
        un_td.appendChild(texte);
        le_tr.appendChild(un_td);
    }
    return le_tr;
};

leTableau.appendAllLines = function() {
    var body_du_tableau = document.createElement("tbody");
    for (var ligne = 0; ligne < leTableau.listeDesLignes.length; ligne++){
        var la_ligne = leTableau.listeDesLignes[ligne]
        var ligne_convertie = leTableau.arrayToTR(la_ligne);
        body_du_tableau.appendChild(ligne_convertie);
        // couleur alterner des lignes
        var test = ligne%2;
        if (test==0){
            body_du_tableau.lastChild.style.backgroundColor = '#b2b2b2';
        }else{
            body_du_tableau.lastChild.style.backgroundColor = '#f6f6f6';
        }
    }
    leTableau.appendChild(body_du_tableau);
}

// dynamiser la couleur du tbody
leTableau.styleTableBody = function(){
    // identifier les tr du tbody
    // definir deux couleur alterne
    var body_du_tableau = document.getElementsByTagName("tbody")[0];
    var les_tr = body_du_tableau.childNodes;
    for(ligne = 0; ligne < les_tr.length; ligne ++){
        var test = ligne%2;
        if (test==0){
            les_tr[ligne].style.backgroundColor = '#b2b2b2';
        }else{
            les_tr[ligne].style.backgroundColor = '#f6f6f6';
        }
    }
}


leTableau.setLeXML = function (an_xml) {
    leTableau.leXML=an_xml;
}

leTableau.getLeXML = function () {
    return leTableau.leXML;
}

leTableau.getColumns = function () {
    // on prend le leXML
    var mon_xml = leTableau.getLeXML();
    // on cherche les tag 'centre'
    var ma_liste_des_centres = mon_xml.getElementsByTagName('centre');
    // on prend le premier
    var un_centre = ma_liste_des_centres[0];
    // on liste ses enfants -> colonnes
    for ( var enfant = 0; enfant < un_centre.children.length; enfant ++) {
        var mon_enfant = un_centre.children[enfant].nodeName;
        leTableau.listeDesEntete.push(mon_enfant);
    }
}

leTableau.isMonEnfantVide = function (une_cellule) {
    if (une_cellule.firstChild == null) {return true;}
    else                                {return false;}
}

leTableau.remplirEnfant = function (un_node) {
    var texte_remplissage = document.createTextNode(" ");
    un_node.appendChild(texte_remplissage);
}

leTableau.getData = function () {
    // on prend le leXML
    var mon_xml = leTableau.getLeXML();
    // on cherche les tag 'centre'
    var ma_liste_des_centres = mon_xml.getElementsByTagName('centre');
    // on les prends un par un 
    for ( var rang_centre = 0; rang_centre < ma_liste_des_centres.length; rang_centre ++) {
	var mes_colonnes = new Array(); 
	var un_centre = ma_liste_des_centres[rang_centre];
	// on liste ses enfants -> colonnes
	for ( var enfant = 0; enfant < un_centre.children.length; enfant ++) {
	    var node_enfant = un_centre.children[enfant];
	    if( leTableau.isMonEnfantVide(node_enfant)){
		leTableau.remplirEnfant(node_enfant);
	    }
	    var mon_enfant = un_centre.children[enfant].firstChild.data;
	    mes_colonnes.push(mon_enfant);
	}
    leTableau.listeDesLignes.push(mes_colonnes);
    }
}



leTableau.onReadyState = function() {
    // This is used by the below funtion, have to declare it before.
    // No more
    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
        leTableau.setLeXML(xhr.responseXML); // <====== here is the point!
        leTableau.getColumns();
        leTableau.appendHead();
        leTableau.getData();
        leTableau.appendAllLines();
        leConteneur.appendChild(leTableau);
    }
}

leTableau.getTheXML = function() {
    xhr = new XMLHttpRequest();
    // the XHR has to be a global thing
    xhr.onreadystatechange = leTableau.onReadyState;
    xhr.onprogress = progression;
    xhr.open("GET", "/projets/projet-javascript-sas/data.xml", true);
    xhr.send(null);
    // From here, this.leXML stores the XML from data.xml
}

// barre de progression
function progression(e){
    var interne = document.getElementById('interne');
    var externe = document.getElementById('externe');
    //var widthProgress = getComputedStyle(interne).width;
    var percentComplete = (e.position / e.totalSize)*100;
    document.getElementById("interne").style.width= (percentComplete*250)/100+'px'; //dynamisation de la longeur de la barre de progression
    externe.style.display = 'block';
    interne.style.display = 'block';
    if(percentComplete==100){
        externe.style.visibility = 'hidden';
        interne.style.visibility = 'Hidden';
    }
}

function afficher()
{
    leConteneur =  document.getElementById('contenu');
    leTableau.getTheXML();
    document.body.removeChild(document.getElementsByTagName('button')[0]);
}



leTableau.filtrer= function()
{
var il_y_eu_suppression=false;

//recuperation des valeurs des inputs
	for(var i = 0;i<chaine_filtres.length;i++){
		var a;
		a=chaine_filtres[i].length-leTableau.children[0].children[0].children[i].children[0].value.length;

		chaine_filtres[i]=leTableau.children[0].children[0].children[i].children[0].value;
		if(a>0) {
			leTableau.deleteTableBody();
			leTableau.appendAllLines(); 
			leTableau.filtrer()	;
			return -1;
		}
	}



	for(var i = 0;i<leTableau.children[1].children.length;i++){
		var une_ligne=leTableau.children[1].children[i]; 
		if(une_ligne==undefined) return -1; /*il n'y a plus de ligne � tester*/
		for(var j = 0;j<chaine_filtres.length;j++) {
			if( ! respect(chaine_filtres[j],une_ligne.children[j].childNodes[0].wholeText)){
			leTableau.children[1].removeChild(une_ligne);						
			il_y_eu_suppression=true;
			break;
			}
		}

	}
	
	leTableau.styleTableBody();		
	if(il_y_eu_suppression) leTableau.filtrer()	;
}


respect = function(a,b)
{
	var reg = new RegExp(a,"gi");
if(b.search(reg)==-1) {
return false; }else return true;

}