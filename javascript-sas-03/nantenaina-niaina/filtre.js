/*

 Filtre :
 -	il faut entrer le mot � rechercher dans le zone de texte
 -	et il faut appuyez la touche entr�e pour le valider

 */

//leTableau.temporaire
leTableau.supprimeLine = function(tableauID) {
    var tbody_supprimer = document.getElementById(tableauID).getElementsByTagName('tbody')[0];
    while (tbody_supprimer.getElementsByTagName('tr').length >= 1) {
        tbody_supprimer.deleteRow(0);
    }
};

// Remplace toutes les occurences d'une chaine
function remplacerTout(entrer, search, repl) {
    while (entrer.indexOf(search) != -1)
        entrer = entrer.replace(search, repl);
    return entrer;
}

// Remplace les caract�res accentu�s
function enleveCaractereSpeciaux(entrer) {
    var norm = new Array('+',"$","^","\\");
    var spec = new Array('', '', '', '');
    for (var i = 0; i < spec.length; i++)
        entrer = remplacerTout(entrer, norm[i], spec[i]);
    return entrer;
}
/*

 fonction filtre du tableau
 -	id : l'identifiant du zone de texte
 -	event : evenement
 -	indice : indice du tableau � trier

 */
leTableau.filtre = function(id, event, indice) {
    if (event.keyCode == 13) {
        var filtre = new Array();
        var mot_rechercher;
        mot_rechercher = new RegExp(enleveCaractereSpeciaux(document.getElementById(id).value.toLowerCase()));
        var j = 0;
        for (var i = 0; i < leTableau.temporaire.length; i++) {
            if (mot_rechercher.test(leTableau.temporaire[i][indice].toLowerCase()) == true) {
                filtre[j] = leTableau.temporaire[i];
                j++;
            }
        }
        leTableau.listeDesLignes = new Array();
        leTableau.listeDesLignes = filtre;
        // supprimer les TR
        leTableau.supprimeLine("tableau");
        //reconstruire le tableau;
        leTableau.appendAllLines();
        delete filtre;
        delete mot_rechercher;
    }
};

//vider le zone de texte
leTableau.zoneTexteVider = function (id) {
    for (var i = 0; i < leTableau.nombre_colonne; i++) {
        document.getElementById("zone_texte_filtre_" + i).value = "";
    }
};