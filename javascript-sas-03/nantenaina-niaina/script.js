/*

 //objet leTableau

 */
var leTableau = document.createElement("table");
leTableau.id = "tableau";
leTableau.listeDesLignes = new Array();
leTableau.temporaire = new Array();
leTableau.ligne_coloree = 0;
leTableau.nombre_colonne = 0;
leTableau.tbody = document.createElement("tbody");
leTableau.arrayToTR = function(un_tableau) {
    var le_tr;
    if (leTableau.ligne_coloree == 0) {
        leTableau.ligne_coloree = 1;
        le_tr = document.createElement("tr");
        le_tr.setAttribute("class", "non_coloree");
    } else {
        leTableau.ligne_coloree = 0;
        le_tr = document.createElement("tr");
        le_tr.setAttribute("class", "coloree");
    }
    for (var colonne = 0; colonne < un_tableau.length; colonne ++) {
        var texte = document.createTextNode(un_tableau[colonne]);
        var un_td = document.createElement("td");
        un_td.appendChild(texte);
        le_tr.appendChild(un_td);
    }
    leTableau.tbody.appendChild(le_tr);
    leTableau.tbody.setAttribute("id", "corps_tableau");
    return leTableau.tbody;
};

//en tete du tableau
leTableau.arrayToTRHeader = function(un_tableau) {
    var le_tr = document.createElement("tr");
    le_tr.className = "entete";
    for (var colonne = 0; colonne < un_tableau.length; colonne ++) {
        var un_td = document.createElement("td");
        if (typeof un_tableau[colonne] == "object") {
            un_td.appendChild(un_tableau[colonne]);
        } else {
            //image pour le tri croissant
            var lien_croissant = document.createElement("img");
            lien_croissant.setAttribute("src", "images/croissant.png");
            lien_croissant.setAttribute("alt", " ");
            lien_croissant.setAttribute("title", "Tri Croissant");
            lien_croissant.setAttribute("onClick", "tri(" + colonne + ",\"CROISSANT\")");
            lien_croissant.setAttribute("class", "croissant");
            //image pour le tri d�croissant
            var lien_decroissant = document.createElement("img");
            lien_decroissant.setAttribute("class", "decroissant");
            lien_decroissant.setAttribute("alt", " ");
            lien_decroissant.setAttribute("title", "Tri Decroissant");
            lien_decroissant.setAttribute("src", "images/decroissant.png");
            lien_decroissant.setAttribute("onClick", "tri(" + colonne + ",\"DECROISSANT\")");
            var span = document.createElement("span");
            span.className = "text_entete";
            var texte = document.createTextNode(un_tableau[colonne]);
            span.appendChild(texte);
            var un_td = document.createElement("td");
            un_td.appendChild(span);
            un_td.appendChild(lien_croissant);
            un_td.appendChild(lien_decroissant);
        }
        le_tr.appendChild(un_td);
    }
    leTableau.appendChild(le_tr);
};


//convertir listeDesLignes en tableau réel 
leTableau.appendAllLines = function() {
    for (var ligne = 0; ligne < leTableau.listeDesLignes.length; ligne++) {
        var la_ligne = leTableau.listeDesLignes[ligne];
        var ligne_convertie = leTableau.arrayToTR(la_ligne);
        leTableau.appendChild(ligne_convertie);
    }
};

//fonction pour le tri croissant et décroissant selon le type
leTableau.invertAllLines = function(indice, type) {
    var montableau = document.getElementById('tableau').getElementsByTagName('tbody')[0];
    var lines = montableau.getElementsByTagName('tr');
    var valeur_trier;
    var listeDesLignesTriee = new Array();
    listeDesLignesTriee.length = 0;
    var i = -1;
    leTableau.ligne = 0;
    while (lines[++i]) {
        listeDesLignesTriee.push([lines[i],lines[i].getElementsByTagName('td')[indice].firstChild ? lines[i].getElementsByTagName('td')[indice].firstChild.data : null]);
    }
    listeDesLignesTriee = appelTri(listeDesLignesTriee, type);
    var j = -1;
    leTableau.ligne_coloree = 0;
    while (listeDesLignesTriee[++j]) {
        if (leTableau.ligne_coloree == 0) {
            leTableau.ligne_coloree = 1;
            listeDesLignesTriee[j][0].setAttribute("class", "non_coloree");
        } else {
            leTableau.ligne_coloree = 0;
            listeDesLignesTriee[j][0].setAttribute("class", "coloree");
        }
        montableau.appendChild(listeDesLignesTriee[j][0]);
    }
};

leTableau.setLeXML = function (an_xml) {
    leTableau.leXML = an_xml;
};

leTableau.getLeXML = function () {
    return leTableau.leXML;
};

//fonction pour inserer le zone de texte pour faire le trie
leTableau.addInputText = function () {
    var mes_colonnes = new Array();
    // on prend le leXML
    var mon_xml = leTableau.getLeXML();
    // on cherche les tag 'centre'
    var ma_liste_des_centres = mon_xml.getElementsByTagName('centre');
    // on prend le premier
    var un_centre = ma_liste_des_centres[0];
    // on liste ses enfants -> colonnes

    for (var enfant = 0; enfant < un_centre.children.length; enfant ++) {
        var input_text = document.createElement('input');
        input_text.setAttribute("type", "text");
        input_text.setAttribute("class", "zone_texte");
        input_text.setAttribute("id", "zone_texte_filtre_" + enfant);
        input_text.setAttribute("name", "texte_" + enfant);
        input_text.setAttribute("onKeyup", "leTableau.filtre(this.id,event," + enfant + ")");
        input_text.setAttribute("onBlur", "leTableau.zoneTexteVider(this.id)");
        input_text.setAttribute("placeholder", "Filtre");
        mes_colonnes.push(input_text);
    }
    leTableau.nombre_colonne = eval(un_centre.children.length);
    leTableau.arrayToTRHeader(mes_colonnes);
    return un_centre;
};

//recup�rer les colonnes
leTableau.getColumns = function (un_centre) {
    var mes_colonnes = new Array();
    // on liste ses enfants -> colonnes
    for (var enfant = 0; enfant < un_centre.children.length; enfant ++) {
        var mon_enfant = un_centre.children[enfant].nodeName;
        mes_colonnes.push(mon_enfant);
    }
    leTableau.arrayToTRHeader(mes_colonnes);
};


//test si le cellule est vide
leTableau.isMonEnfantVide = function (une_cellule) {
    if (une_cellule.firstChild == null) {
        return true;
    }
    else {
        return false;
    }
};

leTableau.remplirEnfant = function (un_node) {
    var texte_remplissage = document.createTextNode(" ");
    un_node.appendChild(texte_remplissage);
};


leTableau.getData = function () {
    // on prend le leXML
    var mon_xml = leTableau.getLeXML();
    // on cherche les tag 'centre'
    var ma_liste_des_centres = mon_xml.getElementsByTagName('centre');
    // on les prends un par un 
    for (var rang_centre = 0; rang_centre < ma_liste_des_centres.length; rang_centre ++) {
        var mes_colonnes = new Array();
        var un_centre = ma_liste_des_centres[rang_centre];
        // on liste ses enfants -> colonnes
        for (var enfant = 0; enfant < un_centre.children.length; enfant ++) {
            var node_enfant = un_centre.children[enfant];
            if (leTableau.isMonEnfantVide(node_enfant)) {
                leTableau.remplirEnfant(node_enfant);
            }
            var mon_enfant = un_centre.children[enfant].firstChild.data;

            mes_colonnes.push(mon_enfant);
        }
        leTableau.listeDesLignes.push(mes_colonnes);
        leTableau.temporaire.push(mes_colonnes);
    }
};


leTableau.getTheXML = function() {
    xhr = new XMLHttpRequest();
    var compteur = 0;
    var sec;

    function mainProgress(e) {
        var percentComplete;
        //variable pour progress_pourcentage
        var objetGraf;
        //variable pour progress
        var objetProgress;

        //recuperation de l'objet progress_pourcentage
        objetGraf = document.getElementById("progress_pourcentage");
        //recuperation de l'objet progress
        objetProgress = document.getElementById("progress");
        //pour afficher le progress_pourcentage
        objetGraf.style.display = 'block';
        //pour afficher le progress
        objetProgress.style.display = 'block';
        //initialisation du percentComplete
        percentComplete = (e.position / e.totalSize) * 100;
        objetGraf.style.width = (800 * Math.round(percentComplete)) / 100 + "px";
        if (e.position == e.totalSize) {
            supprimerBarreProgression();

        }
        if (objetGraf.firstChild) {
            objetGraf.replaceChild(document.createTextNode(Math.round(percentComplete) + " %"), objetGraf.firstChild);
        } else {
            objetGraf.appendChild(document.createTextNode(Math.round(percentComplete) + " %"));
        }
    }

    //pour ne pas afficher le barre de progression � la fin du chargement
    function supprimerBarreProgression() {
        compteur++;
        sec = setTimeout(supprimerBarreProgression, 1000);
        if (compteur == 3) {
            document.getElementById("progress").style.display = 'none';
        }
        if (compteur == 5) {
            document.getElementById("progress_pourcentage").style.display = 'none';
            clearTimeout(sec);
        }
    }

    //evenement onprogress
    xhr.onprogress = mainProgress;
    // the XHR has to be a global thing
    xhr.onreadystatechange = leTableau.onReadyState;
    xhr.open("GET", "/projets/projet-javascript-sas/data.xml", true);
    xhr.send(null);
    // From here, this.leXML stores the XML from data.xml
};

leTableau.onReadyState = function() {
    // This is used by the below funtion, have to declare it before.
    // No more
    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
        leTableau.setLeXML(xhr.responseXML); // <====== here is the point!
        leTableau.getColumns(leTableau.addInputText());
        leTableau.getData();
        leTableau.appendAllLines();
        leConteneur.appendChild(leTableau);
    }
};

//afficher le contenu du XML
function afficher() {
    leConteneur = document.getElementById('contenu');
    leTableau.getTheXML();
}

/*
 //tri croissant et d�croissant selon le type
 type :
 - CROISSANT
 - DECROISSANT
 indice :
 - indice du colonne du tableau
 */
function tri(indice, type) {
    leTableau.invertAllLines(indice, type);
}
