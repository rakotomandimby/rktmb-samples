/*

 //tri qui utilise la méthode sort
	type : 
		-	CROISSANT
		-	DECROISSANT
	strNomArrayListe :	la liste des tableaux à trier
 */
function appelTri(strNomArrayListe, type) {
    if (type == "CROISSANT") {
        strNomArrayListe.sort(trierCroissant);
    } else {
        strNomArrayListe.sort(trierDecroissant);
    }
    return strNomArrayListe;
}

// Remplace toutes les occurences d'une chaine
function remplacerTout(entrer, search, repl) {
    while (entrer.indexOf(search) != -1)
        entrer = entrer.replace(search, repl);
    return entrer;
}

// Remplace les caractères accentués
function enleveAccent(entrer) {
    var norm = new Array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'Þ', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ð', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ý', 'þ', 'ÿ');
    var spec = new Array('A', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', '0', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 'b', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'd', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'b', 'y');
    for (var i = 0; i < spec.length; i++)
        entrer = remplacerTout(entrer, norm[i], spec[i]);
    return entrer;
}

//tri croissant
function trierCroissant(x1, x2) {
    var reg_date = /^(\d{2})[\/\- ](\d{2})[\/\- ](\d{2})/;
    var reg_chiffre = /^[0-9]$/i;
    if (reg_chiffre.test(x1[1]) == true) {
        //convertir string en integer
        x1[1] = parseFloat(x1[1]);
    }
    if (reg_chiffre.test(x2[1]) == true) {
        //convertir string en integer
        x2[1] = parseFloat(x2[1]);
    }

    if (typeof x1[1] != "number") {
        if (typeof x2[1] != "number") {
            if (x1[1] == " ") {
                x1[1] = "zzzzzzzzzzzzzz";
            }
            if (x2[1] == " ") {
                x2[1] = "zzzzzzzzzzzzzz";
            }
            //pour les dates
            x1[1] = x1[1].replace(reg_date, "$3$2$1");
            x2[1] = x2[1].replace(reg_date, "$3$2$1");
            if (enleveAccent(x1[1].toLowerCase()) > enleveAccent(x2[1].toLowerCase())) return 1;
            if (enleveAccent(x1[1].toLowerCase()) < enleveAccent(x2[1].toLowerCase())) return -1;
            return 0;
        } else {
            //pour les chiffres
            if (x1[1] == " ") {
                x1[1] = "999999999";
            }
            if (x2[1] == " ") {
                x2[1] = "999999999";
            }
            return parseFloat(x1[1]) - parseFloat(x2[1]);
        }
    } else {
        //pour les chiffres
        if (x1[1] == " ") {
            x1[1] = "999999999";
        }
        if (x2[1] == " ") {
            x2[1] = "999999999";
        }
        return parseFloat(x1[1]) - parseFloat(x2[1]);
    }
}

//tri d�croissant
function trierDecroissant(x1, x2) {
    //regex pour les dates
    var reg_date = /^(\d{2})[\/\- ](\d{2})[\/\- ](\d{2})/;
    //regex pour les chiffres
    var reg_chiffre = /^[0-9]$/i;
    if (reg_chiffre.test(x1[1]) == true) {
        //convertir string en integer
        x1[1] = parseFloat(x1[1]);
    }
    if (reg_chiffre.test(x2[1]) == true) {
        //convertir string en integer
        x2[1] = parseFloat(x2[1]);
    }

    if (typeof x1[1] != "number") {
        if (typeof x2[1] != "number") {
            if (x1[1] == null) {
                x1[1] = " ";
            }
            if (x2[1] == null) {
                x2[1] = " ";
            }
            //pour les dates
            x1[1] = x1[1].replace(reg_date, "$3$2$1");
            x2[1] = x2[1].replace(reg_date, "$3$2$1");
            if (enleveAccent(x1[1].toLowerCase()) < enleveAccent(x2[1].toLowerCase())) return 1;
            if (enleveAccent(x1[1].toLowerCase()) > enleveAccent(x2[1].toLowerCase())) return -1;
            return 0;
        } else {
            //pour les chiffres
            if (x1[1] == null) {
                x1[1] = -1;
            }
            if (x2[1] == null) {
                x2[1] = -1
            }
            return parseFloat(x2[1]) - parseFloat(x1[1]);
        }
    } else {
        //pour les chiffres
        if (x1[1] == null) {
            x1[1] = -1;
        }
        if (x2[1] == null) {
            x2[1] = -1
        }
        return parseFloat(x2[1]) - parseFloat(x1[1]);
    }
}
	