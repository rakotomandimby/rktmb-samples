/*
----------------------------------------------------------------------
Declaration de l'attribut de la Class leTableau
----------------------------------------------------------------------
*/
var leTableau = document.createElement("table");
leTableau.tableHead = leTableau.appendChild(document.createElement("thead"));
leTableau.listeDesLignes = new Array();
leTableau.listeDesLignesEntete = new Array();

/*
----------------------------------------------------------------------
Tous les Methodes de la Class leTabeau
----------------------------------------------------------------------
*/

//Methode qui insere des elements d'un tableau dans un Element TR 
leTableau.arrayToTR = function(un_tableau) 
{
	var le_tr = document.createElement("tr");
	for (var colonne = 0; colonne < un_tableau.length; colonne ++ )
	{
		var texte = document.createTextNode(un_tableau[colonne]);
		var un_td = document.createElement("td");
		un_td.setAttribute("class","colonne_"+colonne);
		un_td.appendChild(texte);
		le_tr.appendChild(un_td);
	}
	return le_tr;
}


//Methode qui insere des elements d'un tableau dans un Element TH 
leTableau.arrayToTH = function(un_tableau) 
{
	var le_tr = document.createElement("tr");
	for (var colonne = 0; colonne < un_tableau.length; colonne ++ )
	{
		var texte = document.createTextNode(un_tableau[colonne]);
		var un_th = document.createElement("th");
		var img_tri_asc = document.createElement("img");
		var img_tri_dsc = document.createElement("img");
		img_tri_asc.setAttribute('src','images/btn_asc.png');		
		img_tri_asc.setAttribute("class","croissant");
		img_tri_asc.setAttribute("onclick","leTableau.triAscendant("+colonne+")");
		img_tri_dsc.setAttribute('src','images/btn_dsc.png');
		img_tri_dsc.setAttribute("class","decroissant");
		img_tri_dsc.setAttribute("onclick","leTableau.triDescendant("+colonne+")");
		un_th.appendChild(texte);
		un_th.appendChild(img_tri_asc);
		un_th.appendChild(img_tri_dsc);
		le_tr.appendChild(un_th);
	}
	return le_tr;
}

//Ajouter la ligne du titre  du Tableau
leTableau.appendLinesTitle = function() 
{
	var la_ligne = leTableau.listeDesLignesEntete[0];
	var ligne_convertie = leTableau.arrayToTH(la_ligne);
	leTableau.tableHead.appendChild(ligne_convertie);
}

//Ajouter les listes des lignes du Tableau
leTableau.appendAllBodyLines = function() 
{
	var le_tbody = document.createElement("tbody");

	for (var ligne = 0; ligne < leTableau.listeDesLignes.length; ligne++)
	{
		var la_ligne = leTableau.listeDesLignes[ligne]
		var ligne_convertie = leTableau.arrayToTR(la_ligne);
		le_tbody.appendChild(ligne_convertie);
	}
	leTableau.appendChild(le_tbody);
}

//Fonction qui ajout  la valeur pour  la variable leXML
leTableau.setLeXML = function (an_xml) 
{
	leTableau.leXML=an_xml;
}

//Fonction qui prend la valeur pour  la variable leXML
leTableau.getLeXML = function () 
{
	return leTableau.leXML;
}

//Ajouter les colonnes entete dans la ligne entete du tableau
leTableau.getColumns = function () 
{
var mes_colonnes = new Array(); 
	// on prend le leXML
	var mon_xml = leTableau.getLeXML();
	// on cherche les tag 'centre'
	var ma_liste_des_centres = mon_xml.getElementsByTagName('centre');
	// on prend le premier
	var un_centre = ma_liste_des_centres[0];
	// on liste ses enfants -> colonnes
	for ( var enfant = 0; enfant < un_centre.children.length; enfant ++) 
	{
		var mon_enfant = un_centre.children[enfant].nodeName;
		mes_colonnes.push(mon_enfant);
	}
	leTableau.listeDesLignesEntete.push(mes_colonnes);
}

leTableau.isMonEnfantVide = function (une_cellule) 
{
    if (une_cellule.firstChild == null)
	{
		return true;
	}
    else                             
	{
		return false;
	}
}
leTableau.remplirEnfant = function (un_node) 
{
	var texte_remplissage = document.createTextNode(" ");
	un_node.appendChild(texte_remplissage);
}

//Ajouter les colonnes dans les lignes du tableau
leTableau.getData = function () 
{
	// on prend le leXML
	var mon_xml = leTableau.getLeXML();
	// on cherche les tag 'centre'
	var ma_liste_des_centres = mon_xml.getElementsByTagName('centre');
	// on les prends un par un 
	for ( var rang_centre = 0; rang_centre < ma_liste_des_centres.length; rang_centre ++) 
	{
		var mes_colonnes = new Array(); 
		var un_centre = ma_liste_des_centres[rang_centre];
		// on liste ses enfants -> colonnes
		for ( var enfant = 0; enfant < un_centre.children.length; enfant ++) 
		{
			var node_enfant = un_centre.children[enfant];
			if( leTableau.isMonEnfantVide(node_enfant))
			{
				leTableau.remplirEnfant(node_enfant);
			}
			var mon_enfant = un_centre.children[enfant].firstChild.data;
			
			mes_colonnes.push(mon_enfant);
		}
		leTableau.listeDesLignes.push(mes_colonnes);
	}
}

//methode qui assigne une Class � un noeud
leTableau.assignerUneClasse = function (nomNoeud)
{
	var le_tr = document.getElementsByTagName(nomNoeud);
	for ( var compteur = 1; compteur < le_tr.length; compteur++)
	{
		if(compteur%2==0)
		{
			le_tr[compteur].className = "ligne_couleur_2";
		}
		else
		{
			le_tr[compteur].className = "ligne_couleur_1";
		}
	}	
}


leTableau.onReadyState = function() 
{
	// This is used by the below funtion, have to declare it before.
	// No more
	if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
	{
		leTableau.setLeXML(xhr.responseXML); // <====== here is the point!
		leTableau.getColumns();
		leTableau.getData();
		leTableau.appendLinesTitle();
		leTableau.appendAllBodyLines();
		leConteneur.appendChild(leTableau);
	}
	leTableau.assignerUneClasse("tr");
}

leTableau.getTheXML = function() 
{
	xhr = new XMLHttpRequest();
	// the XHR has to be a global thing
	xhr.onreadystatechange = leTableau.onReadyState;
	xhr.onprogress= onProgress;
	xhr.open("GET", "../data.xml", true);
	xhr.send(null);
} 

// tri Croissant sur une colonne
leTableau.triAscendant = function(indice_colonne) 
{
	indice_globale = indice_colonne;
	leTableau.removeChild(leTableau.lastChild);
	leTableau.testIsEmpty(indice_colonne);
	if (testDate(variable_globale) == true)
	{
		leTableau.listeDesLignes.sort(trierDateAsc);
		leTableau.appendAllBodyLines();
		leTableau.assignerUneClasse("tr");
	}
	else 
	{ 
		if (testEntier(variable_globale) == true)
		{
			leTableau.listeDesLignes.sort(trierEntierAsc);
			leTableau.appendAllBodyLines();
			leTableau.assignerUneClasse("tr");
		}
		else 
		{
			leTableau.listeDesLignes.sort(triCaractereAsc);
			leTableau.appendAllBodyLines();
			leTableau.assignerUneClasse("tr");
		}		
	}

}

// tri D�croissant sur une colonne
leTableau.triDescendant = function(indice_colonne) 
{
	indice_globale = indice_colonne;
	leTableau.removeChild(leTableau.lastChild);
	leTableau.testIsEmpty(indice_colonne);
	if (testDate(variable_globale) == true)
	{
		leTableau.listeDesLignes.sort(trierDateDesc);
		leTableau.appendAllBodyLines();
		leTableau.assignerUneClasse("tr");
	}
	else 
	{ 
		if (testEntier(variable_globale) == true)
		{
			leTableau.listeDesLignes.sort(trierEntierDesc);
			 leTableau.appendAllBodyLines();
			 leTableau.assignerUneClasse("tr");
		}
		else 
		{
			leTableau.listeDesLignes.sort(triCaractereDesc);
			leTableau.appendAllBodyLines();
			leTableau.assignerUneClasse("tr");
		}		
	}

}

// on recup�re la premi�re valeur non vide du colonne
leTableau.testIsEmpty = function(indice_globale)
{	
	for(var ligne = 0; ligne< leTableau.listeDesLignes.length; ligne++)
	{
		variable_globale=leTableau.listeDesLignes[ligne][indice_globale];
		if(variable_globale != " ")
		{		
			return (variable_globale);			
			break;
		}
	}
}

/*
*************************************************************
Fonction qui fait un tri croissant des elements du tableau
*************************************************************
*/

//tri chaines de caract�re
function triCaractereAsc(ligne_a,ligne_b)
{
	return (convertStringValue(ligne_a[indice_globale].toLowerCase(),0) < convertStringValue(ligne_b[indice_globale].toLowerCase(),0))? -1 : 1;
}

// Tri date
function trierDateAsc(x1,x2)
{  
	return (convertDateValue(x1[indice_globale],0) < convertDateValue(x2[indice_globale],0))? -1 : 1;
}

// Tri chiffre
function trierEntierAsc(x1,x2)
{  
	return (convertEntierValue(x1[indice_globale],0) < convertEntierValue(x2[indice_globale],0))? -1 : 1;
}

/*
*************************************************************
Fonction qui fait un tri decroissant des elements du tableau
*************************************************************
*/
function triCaractereDesc(ligne_a,ligne_b)
{
	return (convertStringValue(ligne_a[indice_globale].toLowerCase(),1) > convertStringValue(ligne_b[indice_globale].toLowerCase(),1))? -1 : 1;
}

function trierDateDesc(x1,x2)
{  
	// Tri descendant sur cha�ne de caract�res
	return (convertDateValue(x1[indice_globale],1) > convertDateValue(x2[indice_globale],1))? -1 : 1;
}

function trierEntierDesc(x1,x2)
{  
	// Tri descendant sur cha�ne de caract�res
	return (convertEntierValue(x1[indice_globale],1) > convertEntierValue(x2[indice_globale],1))? -1 : 1;
}

/*
*************************************************************
Fonction qui assigne une valeur aux champs vide par rapport
aux choix du tri demand�
*************************************************************
*/

function convertEntierValue(chaine, indicateur)
{
	if(chaine != " ")
	{
		var entier = parseFloat(chaine);
		return(entier);
	}
	else
	{
		if(indicateur == 0)
		{
			var entier = ":"; // le caract�re ":" est plus grand que le chiffre 9 dans le code ASCII
			return(parseInt(entier));
		}
		else
		{
			var entier ="-"; // le caract�re "-" est plus petit que le chiffre 0 dans le code ASCII
			return(parseInt(entier));
		}
	}
}

// Fonction qui va assigner des valeurs sur les chaines vides en fonction du choix du tri
function convertStringValue(chaine, indicateur)
{
	if(chaine != " ")
	{
		return(supprimerAccent(chaine));
	}
	else
	{
		if(indicateur == 0)
		{
			chaine = "|"; // le caract�re "|" est plus grand que la lettre z dans le code ASCII
			return(chaine);
		}
		else
		{
			chaine =" "; // l'espace est consider� plus petit que "a"
			return(chaine);
		}
	}
}

// Fonction qui va convertir en un entier un chaine de carat�re de format DATE
function convertDateValue(chaine, indicateur)
{
	if(chaine != " ")
	{
		var jour = chaine.substring(0,2);
		var mois = chaine.substring(3,5);
		var annee = chaine.substring(6);

		var intdate = parseFloat(annee + mois + jour);
		return(intdate);
	}
	else // On assigne un valeur au champ vide par rapport au choix de tri
	{
		if(indicateur == 0)
		{
			var intdate = 991231;
			return(intdate);
		}
		else
		{
			var intdate =-1;
			return(intdate);
		}
	}
}

// Fonction qui va tester si une chaine de caract�res est un entier
function testEntier(chaine)
{
	if(isNaN(chaine))
	{	
		return false;
	}
	else
	{	
		return true;
	}
}

// Fonction qui va tester si une chaine de caract�re est au format DATE
function testDate(chaine_date)
{
	var reg=new RegExp("[0-9]{2}[/]{1}[0-9]{2}[/]{1}[0-9]{2,4}","g");
	
	if (reg.test(chaine_date) == true) 
	{
		return true;
	}
	else 
	{
		return false;
	}
}

// On supprime les caract�res avec accent
function supprimerAccent(chaine_suppr) 
{
	var accent = new Array('�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�');
	var no_accent = new Array('A','A','A','A','A','A','A','C','E','E','E','E','I','I','I','I','D','N','O','O','O','0','O','O','U','U','U','U','Y','b','s','a','a','a','a','a','a','a','c','e','e','e','e','i','i','i','i','d','n','o','o','o','o','o','o','u','u','u','u','y','y','b','y');
	for (var i = 0; i < no_accent.length; i++)
	{
		chaine_suppr = remplacerCaractere(chaine_suppr, accent[i], no_accent[i]);
	}
	return chaine_suppr;
	
}
// Fonction qui remplace des caract�res
function remplacerCaractere(chaine_comp, accent, no_accent)
{
	while (chaine_comp.indexOf(accent) != -1)
	{
		chaine_comp = chaine_comp.replace(accent, no_accent);
	}
	return chaine_comp;
}

function afficher()
{
	leConteneur =  document.getElementById('contenu');
	leTableau.getTheXML();
	leTableau.assignerUneClasse();
}
//fonction qui calcule la progression
function onProgress(e)
{
	if(e.lengthComputable)
	{
		var percentComplete = (e.position / e.totalSize)*100;
		document.getElementById("deuxieme").style.width=(percentComplete*400)/100+'px';
		document.getElementById("premier").style.display='block';
		document.getElementById("deuxieme").style.display='block';
		if(percentComplete==100) // faire disparaitre la barre de progression
		{
			document.getElementById("premier").style.visibility='hidden';
			document.getElementById("deuxieme").style.visibility='hidden';
		}
	}
}
