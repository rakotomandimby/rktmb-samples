#!/bin/bash

# check the keys with 'xev > /tmp/xev.txt'

for XF86KEY in $(awk '/ XF86/{print $7}' /tmp/xev.txt | sed 's/),//g' |  uniq)
do
    echo "    <keybind key=\"${XF86KEY}\">                             "
    echo "      <action name=\"Execute\">			       "
    echo "        <command>echo ${XF86KEY} > /tmp/command</command>    "
    echo "      </action>					       "
    echo "    </keybind>                                               " 
done