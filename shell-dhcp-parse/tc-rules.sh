
echo ${TC} qdisc del dev ${IFLAN} root
echo ${TC} qdisc del dev ${IFWAN} root


echo ${TC} qdisc add dev ${IFLAN} root handle 1:0 htb default 10
echo ${TC} class add dev ${IFLAN} parent 1:0 classid 1:1 htb rate ${DL_RATE}kbit
echo ${TC} class add dev ${IFLAN} parent 1:1 classid 1:10 htb rate $(( ${DL_RATE} / 10 ))kbit ceil $(( ${DL_RATE} - 500 ))kbit prio 6
echo ${TC} class add dev ${IFLAN} parent 1:1 classid 1:20 htb rate $(( ${DL_RATE} /  5 ))kbit ceil $(( ${DL_RATE} /  2 ))kbit prio 5
echo ${TC} class add dev ${IFLAN} parent 1:1 classid 1:30 htb rate $(( ${DL_RATE} /  4 ))kbit ceil $(( ${DL_RATE} /  2 ))kbit prio 4
echo ${TC} class add dev ${IFLAN} parent 1:1 classid 1:40 htb rate $(( ${DL_RATE} /  5 ))kbit ceil $(( ${DL_RATE} /  2 ))kbit prio 3 # Mail, SSH,...
echo ${TC} class add dev ${IFLAN} parent 1:1 classid 1:50 htb rate $(( ${DL_RATE} /  2 ))kbit ceil $(( ${DL_RATE} /  1 ))kbit prio 2 # Wiki TinyPM VIP
echo ${TC} class add dev ${IFLAN} parent 1:1 classid 1:60 htb rate $(( ${DL_RATE} /  1 ))kbit ceil $(( ${DL_RATE} /  1 ))kbit prio 1
echo ${TC} qdisc add dev ${IFLAN} parent 1:10 handle 10: sfq perturb 10
echo ${TC} qdisc add dev ${IFLAN} parent 1:20 handle 20: sfq perturb 10
echo ${TC} qdisc add dev ${IFLAN} parent 1:30 handle 30: sfq perturb 10
echo ${TC} qdisc add dev ${IFLAN} parent 1:40 handle 40: sfq perturb 10
echo ${TC} qdisc add dev ${IFLAN} parent 1:50 handle 50: sfq perturb 10
echo ${TC} qdisc add dev ${IFLAN} parent 1:60 handle 60: sfq perturb 10


echo ${TC} qdisc add dev ${IFWAN} root handle 1:0 htb default 10
echo ${TC} class add dev ${IFWAN} parent 1:0 classid 1:1 htb rate ${UL_RATE}kbit
echo ${TC} class add dev ${IFWAN} parent 1:1 classid 1:10 htb rate $(( ${UL_RATE} / 10 ))kbit ceil $(( ${UL_RATE} - 100 ))kbit prio 6
echo ${TC} class add dev ${IFWAN} parent 1:1 classid 1:20 htb rate $(( ${UL_RATE} /  4 ))kbit ceil $(( ${UL_RATE} /  2 ))kbit prio 5
echo ${TC} class add dev ${IFWAN} parent 1:1 classid 1:30 htb rate $(( ${UL_RATE} /  4 ))kbit ceil $(( ${UL_RATE} /  2 ))kbit prio 4
echo ${TC} class add dev ${IFWAN} parent 1:1 classid 1:40 htb rate $(( ${UL_RATE} /  5 ))kbit ceil $(( ${UL_RATE} /  2 ))kbit prio 3 # Mail, SSH,...
echo ${TC} class add dev ${IFWAN} parent 1:1 classid 1:50 htb rate $(( ${UL_RATE} /  2 ))kbit ceil $(( ${UL_RATE} /  1 ))kbit prio 2 # Wiki TinyPM VIP
echo ${TC} class add dev ${IFWAN} parent 1:1 classid 1:60 htb rate $(( ${UL_RATE} /  1 ))kbit ceil $(( ${UL_RATE} /  1 ))kbit prio 1
echo ${TC} qdisc add dev ${IFWAN} parent 1:10 handle 10: sfq perturb 10
echo ${TC} qdisc add dev ${IFWAN} parent 1:20 handle 20: sfq perturb 10
echo ${TC} qdisc add dev ${IFWAN} parent 1:30 handle 30: sfq perturb 10
echo ${TC} qdisc add dev ${IFWAN} parent 1:40 handle 40: sfq perturb 10
echo ${TC} qdisc add dev ${IFWAN} parent 1:50 handle 50: sfq perturb 10
echo ${TC} qdisc add dev ${IFWAN} parent 1:60 handle 60: sfq perturb 10
