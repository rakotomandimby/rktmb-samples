#!/bin/bash

IFLAN="eth0"
IFWAN="eth1"

awk -F "," '{print $1,$2,$3,$4}' < /etc/network/users.txt > /tmp/users

TC="/sbin/tc"
IPT="/sbin/iptables"

DL_RATE="3000"
UL_RATE="1000"

# pre run loop

echo ${IPT} -t mangle -F FORWARD 
echo ${IPT} -t mangle -F POSTROUTING
echo ${IPT} -t mangle -F PREROUTING

. /usr/local/bin/tc-rules.sh # charger la definition des classes tc

# On balance tout en basse priorite dans un premier temps
echo ${IPT} -t mangle -A FORWARD -p tcp -d 192.168.0.0/24  -j CLASSIFY --set-class 1:10
echo ${IPT} -t mangle -A FORWARD -p udp -d 192.168.0.0/24  -j CLASSIFY --set-class 1:10
echo ${IPT} -t mangle -A FORWARD -p tcp -s 192.168.0.0/24  -j CLASSIFY --set-class 1:10
echo ${IPT} -t mangle -A FORWARD -p udp -s 192.168.0.0/24  -j CLASSIFY --set-class 1:10

. /etc/network/conf-bandwidth.sh # charger la configuration du bandwidth manager

for SRV in ${SERVEURS_TRAVAIL}
do
    echo ${IPT} -t mangle -A FORWARD -p tcp  -d ${SRV} -s 192.168.0.0/24  -j CLASSIFY --set-class 1:50
    echo ${IPT} -t mangle -A FORWARD -p tcp  -s ${SRV} -d 192.168.0.0/24  -j CLASSIFY --set-class 1:50
    
    echo ${IPT} -t mangle -A FORWARD -p udp  -d ${SRV} -s 192.168.0.0/24  -j CLASSIFY --set-class 1:50
    echo ${IPT} -t mangle -A FORWARD -p udp  -s ${SRV} -d 192.168.0.0/24  -j CLASSIFY --set-class 1:50
done


for P in ${PORTS_TRAVAIL}
do
    echo ${IPT} -t mangle -A FORWARD -p tcp --sport ${P} -d 192.168.0.0/24  -j CLASSIFY --set-class 1:40
    echo ${IPT} -t mangle -A FORWARD -p tcp --dport ${P} -s 192.168.0.0/24  -j CLASSIFY --set-class 1:40
done


for VIP in ${LAN_VIP_USERS}
do
    echo ${IPT} -t mangle -A FORWARD -p tcp -d  ${VIP}/32  -j CLASSIFY --set-class 1:50
    echo ${IPT} -t mangle -A FORWARD -p tcp -s  ${VIP}/32  -j CLASSIFY --set-class 1:50
    echo ${IPT} -t mangle -A FORWARD -p udp -d  ${VIP}/32  -j CLASSIFY --set-class 1:50
    echo ${IPT} -t mangle -A FORWARD -p udp -s  ${VIP}/32  -j CLASSIFY --set-class 1:50
done





