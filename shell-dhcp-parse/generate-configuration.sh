#!/bin/bash

# configuration DHCP
/usr/local/bin/dhcpd-prepend.sh >  /etc/dhcp/dhcpd.conf
/usr/local/bin/dhcpd-body.sh    >> /etc/dhcp/dhcpd.conf
/usr/local/bin/dhcpd-apend.sh   >> /etc/dhcp/dhcpd.conf
service isc-dhcp-server restart

# Bind forward
/usr/local/bin/bind-prepend.sh > /etc/bind/neov.mg.dns
/usr/local/bin/bind-body.sh    >> /etc/bind/neov.mg.dns
# Bind reverse
/usr/local/bin/dnib-prepend.sh > /etc/bind/neov.mg.inv
/usr/local/bin/dnib-body.sh    >> /etc/bind/neov.mg.inv
service bind9 restart


# Bandwidth Management
/usr/local/bin/debit.sh | bash 

# MAC restrictions
/usr/local/bin/bandwidth.sh | bash 
