#!/bin/bash

echo '$TTL    604800                                                         ' 
echo '0.168.192.in-addr.arpa.  IN      SOA     neov.mg.     root.neov.mg.  ( '
echo '                                                  '$(date +%Y%m%d%H)'  '
echo '                                                  604800               '
echo '                                                  86400                '
echo '                                                  2419200              '
echo '                                                  604800               '
echo ')                                                                      '    
echo '        IN       NS     ctrl2.neov.mg.                                 '
echo ''

