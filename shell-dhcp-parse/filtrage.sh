#!/bin/bash

IFLAN="eth0"

awk -F "," '{print $1,$2,$3,$4}' < /etc/network/users.txt > /tmp/users

IPT="/sbin/iptables"
echo ${IPT} -t filter -F FORWARD 

while read ONE_PRIO ONE_NOM ONE_MAC ONE_IP 
do 
    echo ${IPT} -t filter -A FORWARD -s ${ONE_IP}/32 -m mac --mac-source ${ONE_MAC} -i ${IFLAN} -j ACCEPT "# prio ${ONE_PRIO} - ${ONE_NOM}"
done < /tmp/users

echo ${IPT} -t filter -A FORWARD  -i ${IFLAN} -j LOG  --log-prefix "franck: "
echo ${IPT} -t filter -A FORWARD  -i ${IFLAN} -j DROP

