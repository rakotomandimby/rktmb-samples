#!/bin/bash
# This only works thanks to a sane structure of host sections

rm users.txt

for H in $(awk '/^host/{print $2}' < dhcpd.conf); 
do 
    M=$(grep -A 1 " ${H} " dhcpd.conf \
        | awk '/hardware ethernet /{gsub(";","",$3); print $3}') ; # get the MAC address

    A=$(grep -A 2 " ${H} " dhcpd.conf \
        | awk '/fixed-address /{gsub(";","",$2); print $2}') ;     # get the associated IP
    N=$(echo "" | awk -v n=${H} '{print tolower(n);}');
    echo 6,${N},${M},${A}  >> users.txt
done
