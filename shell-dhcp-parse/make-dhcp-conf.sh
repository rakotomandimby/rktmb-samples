#!/bin/bash

awk -F "," '{print $1,$2,$3,$4}' < /etc/network/users.txt > /tmp/users


echo 'authoritative;                                           '        
echo 'ddns-update-style interim;                               '
echo 'ignore client-updates;                                   '
echo 'update-static-leases on;                                 '
echo 'option routers 192.168.0.9;                              '
echo 'option domain-name-servers 192.168.0.9, 208.67.222.222;  '
echo 'default-lease-time 604800;                               '
echo 'max-lease-time 604800;                                   '
echo 'INTERFACES="eth0";                                       '
echo 'deny unknown-clients;                                    '        
echo 'subnet 192.168.0.0 netmask 255.255.255.0 {               '
echo '       range 192.168.0.11 192.168.0.254;                 '
echo '       option broadcast-address 192.168.0.255;           '
echo '                                                         '
echo '                                                         '


while read ONE_PRIO ONE_NOM ONE_MAC ONE_IP 
do 
    
    echo 'host '${ONE_NOM}' {'
    echo 'hardware ethernet '${ONE_MAC}';'
    echo 'fixed-address '${ONE_IP}';'
    echo '}'

done  < /tmp/users

echo '}                                                        '                 
