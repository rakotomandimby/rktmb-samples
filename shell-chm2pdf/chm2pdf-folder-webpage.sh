#!/bin/bash

SOURCE_DIR=$1
DEST_DIR=$2

# Warning! I have multi core system:
# I completely use it!

for CHM_FILE in ${SOURCE_DIR}/*.chm
do 
    chm2pdf --webpage "${CHM_FILE}" &
done