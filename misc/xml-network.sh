#!/bin/bash

echo "<network>                                                   "          
echo "  <name>default</name>                                      "
echo "  <uuid>7efbc96f-94e8-443f-820c-615d2e0331d5</uuid>         "
echo "  <forward mode='nat'/>                                     "
echo "  <bridge name='virbr0' stp='on' delay='0' />               "
echo "  <ip address='172.17.77.1' netmask='255.255.255.0'>        "
echo "    <dhcp>                                                  "
echo "      <range start='172.17.77.2' end='172.17.77.254' />     "
for VM in $(sudo ./vm list --all --name | awk '/^ -/{print $2}') 
do 
    MAC=$(sudo ./vm dumpxml ${VM} | awk -F "'" '/mac address/{print $2}')
    echo '      <host mac="'${MAC}'" name="'${VM}'.vm.mihamina.netapsys.fr" ip="172.17.77.'$(( ${RANDOM} % 250 ))'" />'
done
echo "    </dhcp>                                                 "
echo "  </ip>                                                     "
echo "</network>                                                  "      


# sudo virsh --connect qemu:///system net-dumpxml default \
#    | sed 's/\.vm\.mihamina\.netapsys\.fr//g' \
#    | awk '/host/{print $4,";",$3, "; echo $ip $name"}' | bash
