#!/bin/bash


for VM in $(sudo ./vm list --all --name | awk '/^ -/{print $2}') 
do 
    sudo ./vm dumpxml ${VM} > VM/${VM}.xml
done

for VM in $(sudo ./vm list --all --name | awk '/running$/{print $2}') 
do 
    sudo ./vm dumpxml ${VM} > VM/${VM}.xml
    sudo ./vm destroy ${VM} 
    sudo ./vm undefine ${VM} 
    emacs VM/${VM}.xml
    sudo ./vm define VM/${VM}.xml
    sudo ./vm start ${VM} 
done


for VM in $(sudo ./vm list --all --name | awk '/^ -/{print $2}') 
do 
    sudo ./vm undefine ${VM} 
    sudo ./vm define VM/${VM}.xml
done
