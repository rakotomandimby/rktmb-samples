# $Revision: 1.4 $, $Date: 2010/01/01 22:46:44 $
Summary:	Show tc statistics in a nicer way
Summary(pl.UTF-8):	Analizator statystyk klas tc
Name:		tccs
Version:	0.1
Release:	1
License:	GPL
Group:		Networking
Source0:	tccs
Source1:	tccg
URL:		http://tccs.sourceforge.net/
BuildArch:	noarch
BuildRoot:	%{tmpdir}/%{name}-%{version}-root-%(id -u -n)

%description
Show tc statistics in a nicer way. There is also tc-viewer with
different approach.

%description -l pl.UTF-8
Analizator statystyk klas tc, przedstawiający je w czytelnej formie.
Nieco inne podejście zastosowano w projekcie tc-viewer.

%prep
%setup -q -c -T

%install
rm -rf $RPM_BUILD_ROOT
install -d $RPM_BUILD_ROOT%{_sbindir}

install %{SOURCE0} %{SOURCE1} $RPM_BUILD_ROOT%{_sbindir}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(644,root,root,755)
%attr(755,root,root) %{_sbindir}/*

%define date	%(echo `LC_ALL="C" date +"%a %b %d %Y"`)
%changelog
* %{date} PLD Team <feedback@pld-linux.org>
All persons listed below can be reached at <cvs_login>@pld-linux.org

$Log: tccs.spec,v $
Revision 1.4  2010/01/01 22:46:44  sparky
- BuildRoot capitalization

Revision 1.3  2008/05/27 05:51:36  qboosh
- .UTF-8, pl fix

Revision 1.2  2008-05-20 23:33:54  gotar
- switched to original sources from PLD repo,
- pl summary and desc

Revision 1.1  2007-12-28 14:52:27  arekm
- initial
