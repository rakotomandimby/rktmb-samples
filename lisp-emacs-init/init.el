(setq inhibit-startup-message t)
(global-font-lock-mode t)
(setq font-lock-maximum-decoration t)
(setq line-number-mode t)
(setq column-number-mode t)
(transient-mark-mode t)
(setq stack-trace-on-error t)
(setq default-frame-alist
  '(
     (font . "-*-courier-bold-r-*-*-14-*-*-*-*-*-*-*")
    )
) 
(setq x-select-enable-clipboard-manager nil)

;; indentation
(setq-default indent-tabs-mode nil)
;; indent to 4
(setq standard-indent 4)

;; longues lignes, ne pas wrapper
(set-default 'truncate-lines t)

;; Parenthese matching
(show-paren-mode t)                  ; highlight matching parens, etc
(setq show-paren-style 'parenthesis) ; highlight character, not expression
(setq blink-matching-paren-distance 51200) ; distance to match paren as



(add-to-list 'load-path "/home/mrakotomandimby/Emacs/cfengine-mode")
(add-to-list 'load-path "/home/mrakotomandimby/Emacs")
(add-to-list 'load-path "/home/mrakotomandimby/Emacs/php-mode")

(add-to-list 'auto-mode-alist '("\\.cf\\'" . cfengine3-mode))

;; indent to 4
(setq standard-indent 4)

(require 'php-mode)

(add-to-list 'load-path "/home/mrakotomandimby/rktmb-samples/lisp-geben")
(autoload 'geben "geben" "PHP Debugger on Emacs" t)



;; ECB
(add-to-list 'load-path "/home/mrakotomandimby/Emacs/ecb")
(require 'ecb)
;;(require 'ecb-autoloads)
(setq ecb-tip-of-the-day nil)
(setq ecb-source-path 
  '(
     ;; ("/user@host:/path/to/project/root" "PROJECT")
     ;;  ("/path/to/project/root"           "PROJECT")
     ("/home/mrakotomandimby/Documents/InfraCFEngine/trunk" "CFEngine-Local")
     ("/home/mrakotomandimby/Documents/Eboons/gw-scripts" "Eboons GW")
;;     ("/home/mrakotomandimby/Documents/OMA/depecheprod"                    "Depeches-OMA")
;;     ("/mrakotomandimby@c6-dev-rpm:/home/mrakotomandimby/rpmbuild"         "RPMBuild")
;;     ("/root@centos6-cfengine-00://var/cfengine/inputs"                    "CFEngine-VM")
;;     ("/c65-packaging:/home/mrakotomandimby/rpmbuild"         "RPMBuild")
;;     ("/varnish3@varnish3-recette.pr.netapsys.fr:/etc/varnish/"         "Varnish3-Pr")
   )
)

	

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ecb-options-version "2.40"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(put 'downcase-region 'disabled nil)
