#!/usr/bin/python

config = "/var/www/hgweb.config"

import cgitb
cgitb.enable()


from mercurial.hgweb.hgwebdir_mod import hgwebdir
import mercurial.hgweb.wsgicgi as wsgicgi

application = hgwebdir(config)
wsgicgi.launch(application)


