#!/bin/bash

for REPO in $(find /home/dev53/repos/ -maxdepth 1 -mindepth 1 -type d)
do
    (cd ${REPO}; hg pull;)
done

for REMOTE_DIR in $(echo 'find /var/www/html/projets -type d -name ".hg"' | ssh dev53@dev53.neov.mg | sed 's/\.hg$//g')
do

    LOCAL_DEST_DIR=$(echo ${REMOTE_DIR} | sed 's,/var/www/html/projets,,' | sed 's,/$,,' | sed 's,^/,,' | sed 's,/,-,g')
    LOCAL_DEST_DIR="/home/dev53/repos/"${LOCAL_DEST_DIR}

    if [ ! -d ${LOCAL_DEST_DIR} ]
    then 
        hg clone ssh://dev53@dev53.neov.mg/${REMOTE_DIR} ${LOCAL_DEST_DIR}
    fi

done



#################################################################

for REPO in $(find /home/dev52/repos/ -maxdepth 1 -mindepth 1 -type d)
do
    (cd ${REPO}; hg pull;)
done


for REMOTE_DIR in $(echo 'find /var/www/html/projets  -maxdepth 5 -type d -name ".hg"' | ssh dev5@192.168.0.5 | sed 's/\.hg$//g')
do

    LOCAL_DEST_DIR=$(echo ${REMOTE_DIR} | sed 's,/var/www/html/projets,,' | sed 's,/$,,' | sed 's,^/,,' | sed 's,/,-,g')
    LOCAL_DEST_DIR="/home/dev52/repos/"${LOCAL_DEST_DIR}

    if [ ! -d ${LOCAL_DEST_DIR} ]
    then 
        hg clone ssh://dev5@192.168.0.5/${REMOTE_DIR} ${LOCAL_DEST_DIR}
    fi

done
