/*
 * Copyright (C) 2001-2003  Daniel Kelly
 * Copyright (C) 2009  Peter Pentchev
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include "prips.h"
#include "except.h"

static void fill(int octet[4][256]);

/*****************************************************************/
/* This function parses the exception string and adds all of the */
/* exceptions to the proper place in the exception table.  A '.' */
/* is used to separate octets.  Numbers are separated by any non */
/* digit other than the '.', which has special meaning.          */
/*****************************************************************/
int set_exceptions(char *exp, int octet[4][256])
{
	size_t i;
	int excludeind = 0, bufferind = 0, octind = 0;
	char buffer[4];

	fill(octet);
	for(i = 0; i < strlen(exp) + 1; i++)
	{
		if( isdigit(exp[i]))
		{
			buffer[bufferind] = exp[i];
			bufferind++;
			assert(bufferind != 4); /* potential overflow... */
		}
		else
		{
			if(bufferind)
			{
				buffer[bufferind] = '\0';
				octet[octind][excludeind] = atoi(buffer);
				bufferind = 0;
				excludeind++; 
			}
			
			if(exp[i] == '.')
			{
				octind++;
				excludeind = 0;
			}
		}
	}
	return(0);
}

static void fill(int octet[4][256])
{
	register int i, j;
	
	for(i = 0; i < 4; i++)
		for(j = 0; j < 256; j++)
			octet[i][j] = -1;
}

/*******************************************************************/
/* Compares each octet against the list of exceptions for that     */
/* octet.  If the octet is in the list of exceptions the 'current' */
/* argument is incremented so that the 'current' variable is moved */
/* up one octet.  I hope to God this makes sense... it's late. I'm */
/* tired.                                                          */
/*******************************************************************/
int except(uint32_t *current, int octet[4][256])
{
	register int i, j;

	for(i = 0; i < 4; i++)
	{
		for(j = 0; j < 256; j++)
		{
			switch(i)
			{
			case 0:
				if((int)((*current >> 24) & 0xff) == octet[i][j])
				{
					*current += (uint32_t)(1 << 24) -1;
					return(1);
				}
				break;
			case 1:
				if((int)((*current >> 16) & 0xff) == octet[i][j])
				{
					*current += (uint32_t)(1 << 16) -1;
					return(1);
				}
				break;
			case 2:
				if((int)((*current >> 8) & 0xff) == octet[i][j])
				{
				    *current += (uint32_t)(1 << 8) -1;	
                                    return(1);
				}
				break;
			case 3:
				if((int)(*current & 0xff) == octet[i][j])
                                	return(1);
				break;

			}
		}
	}
	return(0);
}
