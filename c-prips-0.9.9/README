README for prips

prips is a tool that can be used to print all of the IP address on a given
range.  It can enhance the usability of tools that are made to work on only
one host at a time (e.g. whois).

Installation:
-------------
At this time, prips has only been tested on Linux and *BSD systems.
It ought to work on any POSIX-compatible system.  If you port it to
a new platform and feel like sending me a patch, please do so!
I will be very grateful.  

Use tar to extract the source code 'tar xvfz prips.tar.gz'. Enter the prips
directory and type 'make'.  That will create a binary call 'prips'.  Copy
that wherever you like (e.g. /usr/local/bin).

Examples:
---------

Example 1 and 2:
----------------
The following two examples illustrate the most basic use of prips.  The
first example prints all of the addresses between the start IP address
(first argument) and end IP address (second argument).  The second example
uses CIDR notation to achieve the same result.

	[dan@twig /]$ prips 192.168.0.0 192.168.0.255
	192.168.0.0
	192.168.0.1
	...
	192.168.0.255

Do the same with CIDR:

	[dan@twig /]$ prips 192.168/24
	192.168.0.0
	192.168.0.1
	...
	192.168.0.255

Example 3:
----------
We can also use prips to go from an IP range to CIDR notation by using the
-c option.

	[dan@twig /]$ prips -c 192.168.0.0 192.168.0.15
	192.168.0.0/28

Example 4:
----------
The -i option allows us to set the number by which each address is
incremented.  This example uses the -i option to print only the network
addresses of each class C size network in the range 10.8/16.

	[dan@twig /]$ prips -i 256 10.8/16
	10.8.0.0
	10.8.1.0
	10.8.2.0
	...
	10.8.255.0

Example 5:
----------
The last example is a shell script that pings all of the hosts on a (small)
network in parallel.  A one or zero indicates whether the host can be pinged
or not.  The -e options is used in this example to exclude the network
address and broadcast address (0 and 255 in the last octet).  Note that this
script only works on small networks because it uses a *lot* of processes.
There is a race in that if the pings don't return fast enough, you may hit
your process limit...

	#!/bin/sh

	if [ $# != 0 ]; then
		ping -c 1 $1 >/dev/null
   		echo "$1: $?"              
	else
		for i in `prips 207.94.169/24 -e ...0,255`
		do
			exec $0 $i &
		done
	fi		

Feedback:
---------
Please send all feedback to Peter Pentchev <roam@ringlet.net>.  Suggestions,
patches, and bug reports are always appreciated.
