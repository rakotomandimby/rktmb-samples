Summary: print IP addresses of a given range 
Name: prips
Version: 0.9.9
Release: 1
License: GPL
# Group: 
URL: https://gitorious.org/prips/
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root

%description
prips can be used to print all IP addresses of a specified range. 
This allows the enhancement of the usability of tools that have been created 
to work on only one host at a time (e.g. whois).

%prep
%setup -q

%build
make 

%install
%make_install

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
# %doc
%{_bindir}/prips
%{_mandir}/man1/prips.1.gz


%changelog
* Fri Jul 06 2012 Mihamina Rakotomandimby <mihamina@rktmb.org> - 
- Initial build.

